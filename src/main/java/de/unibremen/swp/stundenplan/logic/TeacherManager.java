/*
 * Copyright 2014 AG Softwaretechnik, University of Bremen, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.unibremen.swp.stundenplan.logic;

import java.util.Collection;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.DayTable;
import de.unibremen.swp.stundenplan.data.Paeda;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.data.Timeslot;
import de.unibremen.swp.stundenplan.data.WeekTable;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.gui.MainFrame;
import de.unibremen.swp.stundenplan.persistence.Data;

/**
 * Diese Klasse verwaltet die Lehrer. Es koennen beispielsweise Lehrer hinzugefuegt werden oder Lehrer ausgegeben werden.
 * 
 * @author D. Luedemann, K. Hoelscher
 * @version 0.1
 * 
 */
public final class TeacherManager {

    /**
     * Logger dieser Klasse zum Protokollieren von Ereignissen und Informationen.
     */
    private static final Logger LOGGER = Logger.getLogger(TeacherManager.class.getName());

    /**
     * Privater Konstruktor, der eine Instanziierung dieser Utility-Klasse verhindert.
     */
    private TeacherManager() {
    }

    /**
     * Prueft, ob schon LehrerInnen im Datenbestand vorhanden sind. Falls nicht, werden einige Default-Lehrer angelegt.
     * 
     * @throws DatasetException
     *             falls bei der Erzeugung oder der Verwendung des Persistenzobjektes ein Fehler auftritt
     */
    public static void init() throws DatasetException {
        LOGGER.debug("Checking database for teachers");
        final Collection<Teacher> teachers = Data.getAllTeachers();
        if (teachers.isEmpty()) {
            LOGGER.debug("Creating default teacher.");
            fillDefaultData();
        }
    }

    /**
     * Fuellt den Datenbestand mit drei Lehrern und weist dem Zeitslot 1,1 einen dieser Lehrer zu.
     * 
     * @throws DatasetException
     *             falls ein Fehler beim Aktualisieren des Datenbestandes auftritt
     */
    public static void fillDefaultData() throws DatasetException {
    	if(Data.getTeacherByAcronym("TST") == null){
	    	Teacher teacher = new Teacher();
			String pName = "Test";
			String pAcronym = "TST";
			String pHoursperweek = "30";
			try{
				teacher.setName(pName);
				teacher.setAcronym(pAcronym);
				teacher.setHoursPerWeek(pHoursperweek);
				teacher.setEnable(true);
				WeekTable wt = new WeekTable();
				for (final Weekday weekday : Weekday.values()) {
					final DayTable dayTable = TimetableManager.createDayTable(weekday);
					wt.addDayTable(dayTable);
				}
				Data.addWeekTable(wt);
				teacher.setWeekTable(wt);
				Data.addTeacher(teacher);
				MainFrame.bezeichnung = new JLabel();
				MainFrame.bezeichnung.setText(teacher.getAcronym());
				MainFrame.bezeichnung.updateUI();
		    }catch(IllegalArgumentException  e){
				JOptionPane.showMessageDialog(null, e.getMessage());
			}catch(DatasetException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
    	}
    }


    /**
     * Gibt eine Sammlung aller LehrerInnen zurueck, die von diesem Manager aktuell verwaltet werden.
     * 
     * @return die Sammlung aller LehrerInnen, die aktuell von diesem Manager verwaltet werden
     * @throws DatasetException
     *             falls ein Fehler beim Abfragen des Datenbestandes auftritt
     */
    public static Collection<Teacher> getAllTeachers() throws DatasetException {
        LOGGER.debug("List of all teachers");
        return Data.getAllTeachers();
    }

    /**
     * Gibt die LehrerIn mit dem gegebenen Kuerzel zurueck.
     * 
     * @param acronym
     *            das Kuerzel der gesuchten LehrerIn
     * @return die LehrerIn mit dem gesuchten Kuerzel oder {@code null} falls keine LehrerIn mit dem gegebenen Kuerzel
     *         existiert
     * @throws DatasetException
     *             falls ein Fehler beim Zugriff auf den Datenbestand auftritt
     */
    public static Teacher getTeacherByAcronym(final String acronym) throws DatasetException {
        LOGGER.debug("Teachers for acronym " + acronym);
        return Data.getTeacherByAcronym(acronym);
    }

}
