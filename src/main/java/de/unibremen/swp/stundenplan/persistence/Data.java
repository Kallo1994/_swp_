/*
 * Copyright 2014 AG Softwaretechnik, University of Bremen, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.unibremen.swp.stundenplan.persistence;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import de.unibremen.swp.stundenplan.config.Config;
import de.unibremen.swp.stundenplan.data.Aktivitaet;
import de.unibremen.swp.stundenplan.data.DayTable;
import de.unibremen.swp.stundenplan.data.Paeda;
import de.unibremen.swp.stundenplan.data.Schoolclass;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.data.Timeslot;
import de.unibremen.swp.stundenplan.data.WeekTable;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;

/**
 * Utility-Klasse als konkrete Persistenz-Implementierung. Verwendet JPA 2.0 zum Verwalten des Datenbestandes. Der
 * statische Initialisierer erzeugt einen Entity-Manager fuer die weitere Verwendung. Falls das fehlschlaegt, wird eine
 * {@link IllegalStateException} ausgelaest.
 * 
 * @author K. Hoelscher
 * @version 0.1
 */
public final class Data {

    /**
     * Logger dieser Klasse zum Protokollieren von Ereignissen und Informationen.
     */
    private static final Logger LOGGER = Logger.getLogger(Data.class.getName());

    /*
     * Statischer Initialisierer. Wird vor der ersten Verwendung dieser Klasse ausgefuehrt.
     */
    static {
        try {
            LOGGER.debug("Creating new persistence manager");
            EntityManagerFactory factory;
            factory = Persistence.createEntityManagerFactory(Config.getString("persistence_name", "stundenplan"));
            entityManager = factory.createEntityManager();
        } catch (Exception e) {
            LOGGER.error("Exception while creating new data object!", e);
            throw new PersistenceException("Could not initialize persistence component: " + e.getMessage());
        }

    }

    /**
     * Maximale Anzahl von Zeichen innerhalb eines normalen Strings. Definiert die entsprechende Spaltenbreite in der
     * unterliegenden Datenbank-Tabelle.
     */
    public static final int MAX_NORMAL_STRING_LEN = 255;

    /**
     * Maximale Anzahl von Zeichen, die ein Lehrer-Akronym haben darf. Definiert die entsprechende Spaltenbreite in der
     * unterliegenden Datenbank-Tabelle.
     */
    public static final int MAX_ACRONYM_LEN = 5;

    /**
     * Der Entity-Manager, der die eigentliche Persistierung und Transaktionsverwaltung uebernimmt. Wird von der
     * JPA-Implementierung bereitgestellt und ueber die {@code persistence.xml} konfiguriert.
     */
    private static EntityManager entityManager;

    /**
     * Statischer Initialisierer. Wird unmittelbar vor der ersten Verwendung dieser Hilfsklasse aufgerufen und
     * initialisiert den Entity-Manager.
     */

    /**
     * Privater Konstruktor um eine unnoetige Instanziierung dieser Utility-Klasse zu verhindern.
     */
    private Data() {
    }

    /**
     * Fuegt die gegebene LehrerIn dem Datenbestand hinzu. Loest eine {@link IllegalArgumentException} aus, falls der
     * Parameterwert {@code null} ist.
     * 
     * @param teacher
     *            die hinzuzufuegende LehrerIn
     * 
     * @throws DatasetException
     *             falls beim Hinzufuegen in der unterliegenden Persistenzkomponente ein Fehler auftritt
     */
    public static void addTeacher(final Teacher teacher) throws DatasetException {
        if (teacher == null) {
            throw new IllegalArgumentException("Value of teacher parameter is null");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(teacher);
            entityManager.getTransaction().commit();
            LOGGER.debug(String.format("Teacher %s persisted.", teacher));
        } catch (Exception e) {
            LOGGER.error("Error adding teacher: ", e);
            throw new DatasetException("Error while adding a teacher: " + e.getMessage());
        }
    }

    /**
     * Gibt die LehrerIn zu dem gegebenen Kuerzel zurueck oder {@code null} falls es keine solche LehrerIn gibt oder das
     * gegebene Kuerzel {@code null} oder leer ist.
     * 
     * @param acronym
     *            das Kuerzel der gesuchten LehrerIn
     * @return die LehrerIn zu dem gegebenen Kuerzel oder {@code null} falls es keine LehrerIn mit dem Kuerzel im
     *         Datenbestand gibt oder der Parameterwert ungaeltig war
     * @throws DatasetException
     *             falls ein Fehler bei der Abfrage des Datenbestandes in der unterliegenden Persistenzkomponente
     *             auftritt
     * 
     */
    public static Teacher getTeacherByAcronym(final String acronym) throws DatasetException {
        if (acronym == null || acronym.trim().isEmpty()) {
            return null;
        }
        try {
            return entityManager.find(Teacher.class, acronym);
        } catch (Exception e) {
            LOGGER.error("Exception while getting teacher by acronym " + acronym, e);
            throw new DatasetException("Error while searching a teacher for acronym " + acronym + ": " + e.getMessage());
        }
    }

    /**
     * Gibt die Sammlung aller im Datenbestand befindlichen LehrerInnen zurueck.
     * 
     * @return die Sammlung aller LehrerInnen
     * @throws DatasetException
     *             falls bei der Abfrage des Datenbestandes ein Fehler in der unterliegenden Persistenzkomponente
     *             auftritt
     */
    @SuppressWarnings("unchecked")
    public static Collection<Teacher> getAllTeachers() throws DatasetException {
        try {
            final Query query = entityManager.createQuery("SELECT t FROM Teacher t");
            return (Collection<Teacher>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error("Exception while getting all teachers!", e);
            throw new DatasetException("Error while getting all teachers: " + e.getMessage());
        }
    }
    
    /**
     * Fuegt die gegebenen Paedagogen der Datenbestand hinzu. Loest eine {@link IllegalArgumentException} aus, falls der
     * Parameterwert {@code null} ist.
     * 
     * @param paeda
     *            der hinzuzufuegende Paedagoge
     * 
     * @throws DatasetException
     *             falls beim Hinzufuegen in der unterliegenden Persistenzkomponente ein Fehler auftritt
     */
    public static void addPaeda(final Paeda paeda) throws DatasetException {
        if (paeda == null) {
            throw new IllegalArgumentException("Value of paeda parameter is null");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(paeda);
            entityManager.getTransaction().commit();
            LOGGER.debug(String.format("Teacher %s persisted.", paeda));
        } catch (Exception e) {
            LOGGER.error("Error adding paeda: ", e);
            throw new DatasetException("Error while adding a paeda: " + e.getMessage());
        }
    }

    /**
     * Gibt die PaedagogeIn zu dem gegebenen Kuerzel zurueck oder {@code null} falls es keine solche PaedagogeIn gibt oder das
     * gegebene Kuerzel {@code null} oder leer ist.
     * 
     * @param acronym
     *            das Kuerzel der gesuchten PaedagogeIn
     * @return die PaedagogeIn zu dem gegebenen Kuerzel oder {@code null} falls es keine PaedagogeIn mit dem Kuerzel im
     *         Datenbestand gibt oder der Parameterwert ungaeltig war
     * @throws DatasetException
     *             falls ein Fehler bei der Abfrage des Datenbestandes in der unterliegenden Persistenzkomponente
     *             auftritt
     * 
     */
    public static Paeda getPaedaByAcronym(final String acronym) throws DatasetException {
        if (acronym == null || acronym.trim().isEmpty()) {
            return null;
        }
        try {
            return entityManager.find(Paeda.class, acronym);
        } catch (Exception e) {
            LOGGER.error("Exception while getting paeda by acronym " + acronym, e);
            throw new DatasetException("Error while searching a paeda for acronym " + acronym + ": " + e.getMessage());
        }
    }

    /**
     * Gibt die Sammlung aller im Datenbestand befindlichen PaedagogeInnen zurueck.
     * 
     * @return die Sammlung aller PaedagogeInnen
     * @throws DatasetException
     *             falls bei der Abfrage des Datenbestandes ein Fehler in der unterliegenden Persistenzkomponente
     *             auftritt
     */
    @SuppressWarnings("unchecked")
    public static Collection<Paeda> getAllPaeders() throws DatasetException {
        try {
            final Query query = entityManager.createQuery("SELECT p FROM Paeda p");
            return (Collection<Paeda>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error("Exception while getting all paeders!", e);
            throw new DatasetException("Error while getting all paeders: " + e.getMessage());
        }
    }

    /**
     * Fuegt den gegebenen Tagesplan zum Datenbestand hinzu. Falls der Parameterwert {@code null} ist, wird eine
     * {@link IllegalArgumentException} ausgeloest.
     * 
     * @param dayTable
     *            der hinzuzufuegende Tagesplan
     * @throws DatasetException
     *             falls beim Hinzufuegen ein Fehler in der unterliegenden Persistenzkomponente auftritt
     */
    public static void addDayTable(final DayTable dayTable) throws DatasetException {
        if (dayTable == null) {
            throw new IllegalArgumentException("Value of daytable parameter is null!");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(dayTable);
            entityManager.getTransaction().commit();
            LOGGER.debug("Tagesplan f�r " + dayTable.getWeekday() + " persistiert.");
        } catch (Exception e) {
            LOGGER.error("Exception while adding day table " + dayTable, e);
            throw new DatasetException("Error while adding day table: " + e.getMessage());
        }
    }

    /**
     * Gibt die Liste aller Tagesplaene des Datenbestandes zurueck.
     * 
     * @return die Liste aller Tagesplaene des Datenbestandes
     * @throws DatasetException
     *             falls bei der Abfrage des Datenbestandes ein Fehler in der unterliegenden Persistenzkomponente
     *             auftritt
     */
    @SuppressWarnings("unchecked")
    public static List<DayTable> getDayTables() throws DatasetException {
        try {
            final Query query = entityManager.createQuery("SELECT d FROM DayTable d");
            return (List<DayTable>) query.getResultList();
        } catch (Exception e) {
            LOGGER.error("Exception while getting all day tables!", e);
            throw new DatasetException("Error while getting all day tables: " + e.getMessage());
        }
    }

    /**
     * Aktualisiert die Werte f�r die gegebene Zeiteinheit im Datenbestand.
     * 
     * @param timeslot
     *            die zu aktualisierende Zeiteinheit
     * @throws DatasetException
     *             falls bei der Aktualisierung ein Fehler in der unterliegenden Persistenzkomponente auftritt oder das
     *             gegebene Objekt noch nicht im Datenbestand existiert
     */
    public static void updateTimeslot(final Timeslot timeslot) throws DatasetException {
        if (timeslot == null) {
            throw new IllegalArgumentException("Value of timeslot parameter is null!");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(timeslot);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception while updating the timeslot " + timeslot, e);
            throw new DatasetException("Error while updating timeslot: " + e.getMessage());
        }
    }
    
    /**
     * Aktualisiert die Werte f�r den gegebenen Paedagogischen Mitarbeiter im Datenbestand.
     * 
     * @param timeslot
     *            die zu aktualisierenden Paedagogischen Mitarbeiter
     * @throws DatasetException
     *             falls bei der Aktualisierung ein Fehler in der unterliegenden Persistenzkomponente auftritt oder das
     *             gegebene Objekt noch nicht im Datenbestand existiert
     */
    public static void updatePaeda(final Paeda paeda) throws DatasetException {
        if (paeda == null) {
            throw new IllegalArgumentException("Value of paeda parameter is null!");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(paeda);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception while updating the paeda " + paeda, e);
            throw new DatasetException("Error while updating paeda: " + e.getMessage());
        }
    }
    
    public static void updateTeacher(final Teacher teacher) throws DatasetException {
        if (teacher == null) {
            throw new IllegalArgumentException("Value of teacher parameter is null!");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(teacher);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception while updating the teacher " + teacher, e);
            throw new DatasetException("Error while updating teacher: " + e.getMessage());
        }
    }
    
    public static void updateAktivitaet(final Aktivitaet ak) throws DatasetException {
        if (ak == null) {
            throw new IllegalArgumentException("Value of Aktivitaet parameter is null!");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(ak);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception while updating the Aktivitaet " + ak, e);
            throw new DatasetException("Error while updating Aktivitaet: " + e.getMessage());
        }
    }
    
    public static void updateSchoolclass(final Schoolclass sc) throws DatasetException {
        if (sc == null) {
            throw new IllegalArgumentException("Value of Schoolclass parameter is null!");
        }
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(sc);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            LOGGER.error("Exception while updating the Schoolclass " + sc, e);
            throw new DatasetException("Error while updating Schoolclass: " + e.getMessage());
        }
    }
    
    /**
      * Fuegt die gegebene Schulklasse der Datenbestand hinzu. Loest eine {@link IllegalArgumentException} aus, falls der
      * Parameterwert {@code null} ist.
      * 
      * @param schoolclass
      *            die hinzuzufuegende Schulklasse
      * 
      * @throws DatasetException
      *             falls beim Hinzufuegen in der unterliegenden Persistenzkomponente ein Fehler auftritt
      */
     public static void addSchoolclass(final Schoolclass schoolclass) throws DatasetException {
         if (schoolclass == null) {
             throw new IllegalArgumentException("Value of paeda schoolclass is null");
         }
         try {
             entityManager.getTransaction().begin();
             entityManager.persist(schoolclass);
             entityManager.getTransaction().commit();
             LOGGER.debug(String.format("Schoolclass %s persisted.", schoolclass));
         } catch (Exception e) {
             LOGGER.error("Error adding schoolclass: ", e);
             throw new DatasetException("Error while adding a schoolclass: " + e.getMessage());
         }
     }
     
     /**
      * Fuegt die gegebene Wochenplan der Datenbestand hinzu. Loest eine {@link IllegalArgumentException} aus, falls der
      * Parameterwert {@code null} ist.
      * 
      * @param weekTable
      *            die hinzuzufuegende Wochenpan
      * 
      * @throws DatasetException
      *             falls beim Hinzufuegen in der unterliegenden Persistenzkomponente ein Fehler auftritt
      */
     public static void addWeekTable(final WeekTable weekTable) throws DatasetException {
         if (weekTable == null) {
             throw new IllegalArgumentException("Value of weekTable parameter is null");
         }
         try {
             entityManager.getTransaction().begin();
             entityManager.persist(weekTable);
             entityManager.getTransaction().commit();
             LOGGER.debug(String.format("weekTable %s persisted.", weekTable));
         } catch (Exception e) {
             LOGGER.error("Error adding weekTable: ", e);
             throw new DatasetException("Error while adding a weekTable: " + e.getMessage());
         }
     }

     /**
      * Fuegt die gegebene TimeSlot der Datenbestand hinzu. L�st eine {@link IllegalArgumentException} aus, falls der
      * Parameterwert {@code null} ist.
      * 
      * @param timeSlot
      *            die hinzuzufuegende TimeSlot
      * 
      * @throws DatasetException
      *             falls beim Hinzufuegen in der unterliegenden Persistenzkomponente ein Fehler auftritt
      */
     public static void addTimeslot(final Timeslot timeSlot) throws DatasetException {
         if (timeSlot == null) {
             throw new IllegalArgumentException("Value of timeSlot parameter is null");
         }
         try {
             entityManager.getTransaction().begin();
             entityManager.persist(timeSlot);
             entityManager.getTransaction().commit();
             LOGGER.debug(String.format("timeSlot %s persisted.", timeSlot));
         } catch (Exception e) {
             LOGGER.error("Error adding timeSlot: ", e);
             throw new DatasetException("Error while adding a timeSlot: " + e.getMessage());
         }
     }
     
     /**
      * Fuegt die gegebene Aktivitaet der Datenbestand hinzu. Loest eine {@link IllegalArgumentException} aus, falls der
      * Parameterwert {@code null} ist.
      * 
      * @param aktivitaet
      *            die hinzuzufuegende Aktivitaet
      * 
      * @throws DatasetException
      *             falls beim Hinzufuegen in der unterliegenden Persistenzkomponente ein Fehler auftritt
      */
     public static void addAktivitaet(final Aktivitaet aktivitaet) throws DatasetException {
         if (aktivitaet == null) {
             throw new IllegalArgumentException("Value of aktivitaet parameter is null");
         }
         try {
             entityManager.getTransaction().begin();
             entityManager.persist(aktivitaet);
             entityManager.getTransaction().commit();
             LOGGER.debug(String.format("aktivitaet %s persisted.", aktivitaet));
         } catch (Exception e) {
             LOGGER.error("Error adding timeSlot: ", e);
             throw new DatasetException("Error while adding a aktivitaet: " + e.getMessage());
         }
     }

     /**
      * Gibt die PaedagogeIn zu dem gegebenen Kuerzel zurueck oder {@code null} falls es keine solche PaedagogeIn gibt oder das
      * gegebene Kuerzel {@code null} oder leer ist.
      * 
      * @param acronym
      *            das Kuerzel der gesuchten PaedagogeIn
      * @return die PaedagogeIn zu dem gegebenen Kuerzel oder {@code null} falls es keine PaedagogeIn mit dem Kuerzel im
      *         Datenbestand gibt oder der Parameterwert ungueltig war
      * @throws DatasetException
      *             falls ein Fehler bei der Abfrage des Datenbestandes in der unterliegenden Persistenzkomponente
      *             auftritt
      * 
      */
     public static Schoolclass getSchoolclassByName(final String name) throws DatasetException {
         if (name == null || name.trim().isEmpty()) {
             return null;
         }
         try {
             return entityManager.find(Schoolclass.class, name);
         } catch (Exception e) {
             LOGGER.error("Exception while getting schoolclass by name " + name, e);
             throw new DatasetException("Error while searching a schoolclass for name " + name + ": " + e.getMessage());
         }

     }

     /**
      * Gibt die Sammlung aller im Datenbestand befindlichen Schulklassen zurueck.
      * 
      * @return die Sammlung aller Schulklassen
      * @throws DatasetException
      *             falls bei der Abfrage des Datenbestandes ein Fehler in der unterliegenden Persistenzkomponente
      *             auftritt
      */
     @SuppressWarnings("unchecked")
     public static Collection<Schoolclass> getAllSchoolclasses() throws DatasetException {
         try {
             final Query query = entityManager.createQuery("SELECT s FROM Schoolclass s");
             return (Collection<Schoolclass>) query.getResultList();
         } catch (Exception e) {
             LOGGER.error("Exception while getting all schooclass!", e);
             throw new DatasetException("Error while getting all schooclass: " + e.getMessage());
         }
     }

}
