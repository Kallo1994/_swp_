package de.unibremen.swp.stundenplan.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.DayTable;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.data.WeekTable;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.logic.TimetableManager;
import de.unibremen.swp.stundenplan.persistence.Data;

public class FrameLehrer extends JFrame {

	/**
	 * Die eineindeutige Kennzeichnung f�r Serialisierung.
	 */
	private static final long serialVersionUID = -5067782815470037302L;

	/**
	 * Dient dazu das der Benutzer die Dtaen eingeben kann
	 */
	private JTextField acronym, name, hoursperweek;

	/**
	 * Zeigt informnationen an, was der benutzer bei den text fields eingeben
	 * soll.
	 */
	private JLabel jName, jAcronym, jHoursperweek;

	/**
	 * Zum best�tigen der eingaben durch den benutzer
	 */
	private JButton bestaetigen;

	/**
	 * Erstellt ein neues Fenster zum hinzufuegen eines Lehrers.
	 */
	public FrameLehrer() {
		super();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Lehrer hinzuf�gen");
		setResizable(false);
		setVisible(true);

		bestaetigen = new JButton("Best�tigen");
		name = new JTextField("");
		acronym = new JTextField("");
		hoursperweek = new JTextField("");
		jName = new JLabel("Der Name des Lehrers");
		jAcronym = new JLabel("Das Acronym des Lehrers");
		jHoursperweek = new JLabel("Die Wochenstundezahl");

		name.setSize(100, 20);
		jName.setSize(200, 20);
		acronym.setSize(100, 20);
		jAcronym.setSize(200, 20);
		hoursperweek.setSize(100, 20);
		jHoursperweek.setSize(200, 20);
		bestaetigen.setSize(MainFrame.BUTTONBREITE, MainFrame.BUTTONHOEHE);

		jName.setLocation(120, 30);
		name.setLocation(150, 60);

		jAcronym.setLocation(120, 80);
		acronym.setLocation(150, 110);

		jHoursperweek.setLocation(130, 130);
		hoursperweek.setLocation(150, 160);

		bestaetigen.setLocation(125, 190);

		Handler handler = new Handler();

		bestaetigen.addActionListener(handler);

		add(name);
		add(acronym);
		add(hoursperweek);
		add(bestaetigen);
		add(jAcronym);
		add(jHoursperweek);
		add(jName);

		pack();

	}

	/*
	 * non javadoc
	 */
	private class Handler implements ActionListener {

		/**
		 * Liest die eingabend es benutzers und f�gt das object der db hinzu.
		 * bei fehlern wird eine exception geworfen.
		 */
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == bestaetigen) {
				Teacher teacher = new Teacher();
				String pName = name.getText();
				String pAcronym = acronym.getText();
				String pHoursperweek = hoursperweek.getText();
				try {
					if (Data.getTeacherByAcronym(pAcronym) != null
							|| Data.getPaedaByAcronym(pAcronym) != null) {
						JOptionPane
								.showMessageDialog(null,
										"Einem P�dagogen oder einen Lehrer mit dem K�rzel gibt es schon!");
						return;
					}
					teacher.setName(pName);
					teacher.setAcronym(pAcronym);
					teacher.setHoursPerWeek(pHoursperweek);
					WeekTable wt = new WeekTable();
					for (final Weekday weekday : Weekday.values()) {
						final DayTable dayTable = TimetableManager
								.createDayTable(weekday);
						wt.addDayTable(dayTable);
					}
					Data.addWeekTable(wt);
					teacher.setWeekTable(wt);
					teacher.setEnable(true);
					Data.addTeacher(teacher);
					setVisible(false);
					dispose();
					MainFrame.updateGUI();
					JOptionPane.showMessageDialog(null,
							"Lehrer erfolgreich erstellt");
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				} catch (DatasetException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}

			}
		}
	}

}
