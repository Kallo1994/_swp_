/*
 * Copyright 2014 AG Softwaretechnik, University of Bremen, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.unibremen.swp.stundenplan.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import de.unibremen.swp.stundenplan.Stundenplan;
import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.Aktivitaet;
import de.unibremen.swp.stundenplan.data.Paeda;
import de.unibremen.swp.stundenplan.data.Schoolclass;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.data.Timeslot;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.logic.TimetableManager;
import de.unibremen.swp.stundenplan.persistence.Data;

/**
 * Das Hauptfenster, in dem die GUI dargestellt wird.
 * 
 */
public final class MainFrame extends JFrame {

	/**
	 * Ein eigener Maushorcher f�r die Tabelle.
	 * 
	 * @author D. L�demann
	 * @version 0.1
	 * 
	 */
	private class MyMouseListener extends MouseAdapter {
		@Override
		public void mousePressed(final MouseEvent event) {
			final int row = table.rowAtPoint(event.getPoint());
			final int col = table.columnAtPoint(event.getPoint());
			if (row >= 0 && row < table.getRowCount() && col >= 1
					&& col < table.getColumnCount()) {
				table.changeSelection(row, col, false, false);
			} else {
				table.clearSelection();
			}
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
			event.consume();
		}

		@Override
		public void mouseReleased(final MouseEvent event) {
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Prueft, ob es sich bei dem gegebenen Mausereignis um einen
		 * Rechtsklick handelt. Falls das so ist, wird ein entsprechendes
		 * Popup-Menu an den durch das Mausereignis uebermittelten Koordinaten
		 * geoeffnet.
		 * 
		 * Vermeidet Redundanz in den Listenern fuer mouse-pressed und
		 * mouse-released-Ereignisse. Beide Listener sind noetig, da Windoof den
		 * Popup-Trigger erst auf Loslassen der Maustaste meldet, Linux und
		 * MacOs aber bereits beim Klicken der Maus.
		 * 
		 * @param event
		 *            das zu pruefende Mausevent
		 * @throws DatasetException
		 */
		private void checkPopup(final MouseEvent event) throws DatasetException {
			final int row = table.rowAtPoint(event.getPoint());
			final int col = table.columnAtPoint(event.getPoint());
			if (event.isPopupTrigger()) {
				/*
				 * Verhindert den nochmaligen Aufruf unter Linux und MacOs.
				 */
				event.consume();
				if (event.getComponent() instanceof JTable && row >= 0
						&& col >= 1) {
					final JPopupMenu popup = createPopup(row, col);
					popup.show(table, event.getX(), event.getY());
				}
			}
		}
	}
	
	/**
	 * Erzeugt ein Popup zum Hinzufuegen einer Aktivitaet.
	 * 
	 * @param row
	 *            Die Zeile.
	 * @param col
	 *            Die Spalte.
	 * @return das neue Popup-Menu
	 * @throws DatasetException
	 */
	private JPopupMenu createPopup(final int row, final int col)
			throws DatasetException {
		final JPopupMenu popmen = new JPopupMenu();
		final JMenuItem menu1 = new JMenuItem("AddAktivitaet");
		final JMenuItem menu2 = new JMenuItem("DeleteAktivitaet");
		final JMenuItem menu3 = new JMenuItem("EditAktivitaet");
		menu1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event){
				final FrameAktivitaet addFrame = new FrameAktivitaet(null);
				addFrame.setTimeslot((Timeslot)table.getValueAt(row, col), row,Weekday.values()[col-1]);
				addFrame.pack(); 
				addFrame.setVisible(true); 
				addFrame.setSize(500, 400);
			}
		});
		menu2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Timeslot ts = (Timeslot)table.getValueAt(row, col);
				try {
					Aktivitaet ak = ts.getAktivitaet();
					List<Teacher> teachers = ak.getTeachers();
					List<Paeda> paedas = ak.getPaedas();
					List<Schoolclass> classes = ak.getClasses();
					String aco = MainFrame.bezeichnung.getText();
					Schoolclass sClass = null;
					Teacher teacher = null;
					Paeda paeda = null;
					if(aco.length() == 2){
						sClass = Data.getSchoolclassByName(aco);
					}else if(Data.getTeacherByAcronym(aco) != null){
						teacher = Data.getTeacherByAcronym(aco);
					}else if(Data.getPaedaByAcronym(aco) != null){
						paeda = Data.getPaedaByAcronym(aco);
					}
					for(Teacher t : teachers){
						Timeslot tsTeach = t.getWeekTable().getDayTableforDay(Weekday.values()[col-1]).getTimeslot(row);
						Aktivitaet akTeach = tsTeach.getAktivitaet();
						if(sClass != null){
							akTeach.removeClass(sClass);
						}
						if(teacher != null){
							akTeach.removeTeacher(teacher);
						}
						if(paeda != null){
							akTeach.removePaeda(paeda);
						}
						if(akTeach.getClasses().size() == 0 && akTeach.getPaedas().size() == 0
								&& akTeach.getTeachers().size() == 0){
							tsTeach.loescheAktivitaet();
						}else{
							Data.updateAktivitaet(akTeach);
						}
						Data.updateTimeslot(tsTeach);
					}
					for(Paeda p : paedas){
						Timeslot tspaedas = p.getWeekTable().getDayTableforDay(Weekday.values()[col-1]).getTimeslot(row);
						Aktivitaet akpaedas = tspaedas.getAktivitaet();
						if(sClass != null){
							akpaedas.removeClass(sClass);
						}
						if(teacher != null){
							akpaedas.removeTeacher(teacher);
						}
						if(paeda != null){
							akpaedas.removePaeda(paeda);
						}
						if(akpaedas.getClasses().size() == 0 && akpaedas.getPaedas().size() == 0
								&& akpaedas.getTeachers().size() == 0){
							tspaedas.loescheAktivitaet();
						}else{
							Data.updateAktivitaet(akpaedas);
						}
						Data.updateTimeslot(tspaedas);
					}
					for(Schoolclass s : classes){
						Timeslot tsclasses = s.getWeekTable().getDayTableforDay(Weekday.values()[col-1]).getTimeslot(row);
						Aktivitaet akclasses = tsclasses.getAktivitaet();
						if(sClass != null){
							akclasses.removeClass(sClass);
						}
						if(teacher != null){
							akclasses.removeTeacher(teacher);
						}
						if(paeda != null){
							akclasses.removePaeda(paeda);
						}
						if(akclasses.getClasses().size() == 0 && akclasses.getPaedas().size() == 0
								&& akclasses.getTeachers().size() == 0){
							tsclasses.loescheAktivitaet();
						}else{
							Data.updateAktivitaet(akclasses);
						}
						Data.updateTimeslot(tsclasses);
					}
					ts.loescheAktivitaet();
					TimetableManager.getTimeslotAt(Weekday.values()[col-1], row);
					Data.updateTimeslot(ts);
					updateGUI();
				} catch (DatasetException e) {
				}
			}
		});
		menu3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Timeslot ts = (Timeslot)table.getValueAt(row, col);
				final FrameAktivitaet addFrame = new FrameAktivitaet(ts.getAktivitaet().getBezeichnung());
				addFrame.setTimeslot((Timeslot)table.getValueAt(row, col), row,Weekday.values()[col-1]);
				addFrame.pack(); 
				addFrame.setVisible(true); 
				addFrame.setSize(500, 400);
			}
		});
		Timeslot ts = TimetableManager.getTimeslotAt(Weekday.values()[col-1], row);
		if (ts.getAktivitaet() == null)
			popmen.add(menu1);
		else {
			popmen.add(menu2);
			popmen.add(menu3);
		}
		popmen.setVisible(true);
		return popmen;
	}

	/**
	 * die einheitliche breite eines Buttons
	 */
	public static final int BUTTONBREITE = 150;

	/**
	 * Die einheitliche L�nge eines Buttons
	 */
	public static final int BUTTONHOEHE = 50;

	/**
	 * Visualisiert einen Wochenplan einer bestimmten person oder Klasse.
	 */
	private final JTable table;

	/**
	 * Dies sind unsere drei Tabellen, die alle Lehrer P�dagogen und Klassen anzeigen.
	 */
	public static JTable table_klassen, table_lehrer, table_paedas;

	/**
	 * Der Dialog, der aufpopt, um einen Lehrer hinzuzufuegen.
	 */
	private final addAktivitaetDialog addAktivitaetDialog;

	/**
	 * Die generierte serialVersionUID.
	 */
	private static final long serialVersionUID = 8285305580202003358L;

	/**
	 * Die Zeilenhoehe einer Tabellenzeile.
	 */
	private static final int ROW_HEIGHT = 40;

	/**
	 * Mit diesen Button lassen sich personen und klassen hinzuf�gen.
	 */
	private JButton addLehrer, addPaeda, addKlasse;

	/**
	 * Das Aktuelle Objekt was man sich anschaut
	 */
	public static JLabel bezeichnung = null;

	/**
	 * Der Konstruktor des Frames. Hier werden die wesentlichen Eigenschaften
	 * der Darstellung des Frames definiert. Die JTable wird angelegt und dem
	 * Frame hinzugefuegt. Es werden die Darstellungseigenschaften der JTable
	 * festgelegt.
	 * 
	 */
	public MainFrame() {
		super();
		if(bezeichnung == null){
			boolean ist = false;
			try {
				for(Teacher t : Data.getAllTeachers()){
					if(t.isEnable()){
						bezeichnung = new JLabel(t.getAcronym());
						ist = true;
						break;
					}
				}
				if(!ist){
					for(Paeda p : Data.getAllPaeders()){
						if(p.isEnable()){
							bezeichnung = new JLabel(p.getAcronym());
							ist = true;
							break;
						}
					}
				}
				if(!ist){
					for(Schoolclass sc : Data.getAllSchoolclasses()){
						if(sc.isEnable()){
							bezeichnung = new JLabel(sc.getName());
							ist = true;
							break;
						}
					}
				}
				if(!ist){
					Teacher tst = Data.getTeacherByAcronym("TST");
					tst.setEnable(true);
					Data.updateTeacher(tst);
					bezeichnung = new JLabel(tst.getAcronym());
				}
			} catch (DatasetException e){
			}
		}
		this.setLayout(new BorderLayout(800, 600));

		addAktivitaetDialog = new addAktivitaetDialog(this);
		table = new JTable(new TimetableModel());
		bezeichnung.setLocation(780, 380);
		bezeichnung.setSize(200, 20);
		add(bezeichnung);

		String[] col = { "Teacher" };
		Object[][] data = new Object[1][1];
		try {
			int size = 0;
			ArrayList<Teacher> teachers = new ArrayList<>();
			for(Teacher t : Data.getAllTeachers()){
				if(t.isEnable()){
					teachers.add(t);
					size++;
				}
			}
			data = new Object[size][1];
			int counter = 0;
			for (int i = 0; i < size; i++) {
				counter = 0;
				for (Teacher t : teachers){
					if (counter == i) {
						if(!t.isEnable()){
							break;
						}
						data[i][0] = t.getName() + " (" + t.getAcronym() + ")";
					}
					counter++;
				}
			}
		} catch (DatasetException e) {
			e.printStackTrace();
		}
		
		String[] colClass = { "Klasse" };
		Object[][] dataClass = new Object[1][1];
		try {
			int size = 0;
			ArrayList<Schoolclass> sClasses = new ArrayList<>();
			for(Schoolclass s : Data.getAllSchoolclasses()){
				if(s.isEnable()){
					sClasses.add(s);
					size++;
				}
			}
			dataClass = new Object[size][1];
			int counter = 0;
			for (int i = 0; i < size; i++) {
				counter = 0;
				for (Schoolclass s : sClasses){
					if (counter == i) {
						if(!s.isEnable()){
							break;
						}
						dataClass[i][0] = s.getName();
					}
					counter++;
				}
			}
		} catch (DatasetException e) {
			e.printStackTrace();
		}
		
		String[] colPeda = { "P�dagogen" };
		Object[][] dataPeda = new Object[1][1];
		try {
			int size = 0;
			ArrayList<Paeda> paedas = new ArrayList<>();
			for(Paeda p : Data.getAllPaeders()){
				if(p.isEnable()){
					paedas.add(p);
					size++;
				}
			}
			dataPeda = new Object[size][1];
			int counter = 0;
			for (int i = 0; i < size; i++) {
				counter = 0;
				for (Paeda p : paedas){
					if (counter == i) {
						if(!p.isEnable()){
							break;
						}
						dataPeda[i][0] = p.getName() + " (" + p.getAcronym() + ")";
					}
					counter++;
				}
			}
		} catch (DatasetException e) {
			e.printStackTrace();
		}
		
		TableModel modelTeacher = new DefaultTableModel(data, col);
		TableModel modelClass = new DefaultTableModel(dataClass, colClass);
		TableModel modelPeda = new DefaultTableModel(dataPeda, colPeda);
		 
		table_klassen = new JTable(modelClass){
				public boolean isCellEditable(int x, int y) {
	                return false;
	            }
	        };

		table_lehrer = new JTable(modelTeacher){
			public boolean isCellEditable(int x, int y) {
                return false;
            }
        };
        
        table_paedas = new JTable(modelPeda){
			public boolean isCellEditable(int x, int y) {
                return false;
            }
        };
        
		add(table_klassen);
		table_klassen.setLocation(20, 375);
		table_klassen.setSize(220, 190);

		add(table_lehrer);
		table_lehrer.setLocation(250, 375);
		table_lehrer.setSize(220, 190);

		add(table_paedas);
		table_paedas.setLocation(480, 375);
		table_paedas.setSize(220, 190);
		/* Ende der Tabellen */

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("BoB's TooL (SWP-I)");
		
		MouseListeners ms = new MouseListeners();
		
		table.addMouseListener(new MyMouseListener());
		table_paedas.addMouseListener(ms.getPaedaMouse());
		table_klassen.addMouseListener(ms.getClassMouse());
		table_lehrer.addMouseListener(ms.getTeacherMouse());

		table.setDefaultRenderer(Timeslot.class, new TimetableRenderer());
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		final JScrollPane scrollPane = new JScrollPane(table);

		/* Die 3 Hinzuf�gen kn�pfe.. */

		addLehrer = new JButton("Lehrer hinzuf�gen");
		addPaeda = new JButton("P�da. Mitarbeiter hinzuf�gen");
		addKlasse = new JButton("Klassehinzuf�gen");

		addLehrer.setSize(150, 40);
		addPaeda.setSize(150, 40);
		addKlasse.setSize(150, 40);

		addLehrer.setLocation(720, 420);
		addPaeda.setLocation(720, 470);
		addKlasse.setLocation(720, 520);

		addLehrer.setVisible(true);
		addPaeda.setVisible(true);
		addKlasse.setVisible(true);

		Handler handler = new Handler();
		addLehrer.addActionListener(handler);
		addPaeda.addActionListener(handler);
		addKlasse.addActionListener(handler);

		getContentPane().add(addLehrer);
		getContentPane().add(addPaeda);
		getContentPane().add(addKlasse);

		/* ...ende */

		/*
		 * 3 unn�tige JLabels weil wir zu dumm sind es in der tabelle zu machen
		 * und ein cooles JLabel was man sich gerade anguckt
		 */

		JLabel current_viewedSP = new JLabel("Betrachteter Wochenplan:");
		JLabel label_klassen = new JLabel("Klassen");
		JLabel label_lehrer = new JLabel("Lehrer");
		JLabel label_paedas = new JLabel("Paeda. Mitarbeiter");
		current_viewedSP.setSize(BUTTONBREITE, 20);
		label_klassen.setSize(BUTTONBREITE, 20);
		label_lehrer.setSize(BUTTONBREITE, 20);
		label_paedas.setSize(BUTTONBREITE, 20);
		this.add(current_viewedSP);
		this.add(label_klassen);
		this.add(label_lehrer);
		this.add(label_paedas);
		
		current_viewedSP.setLocation(730, this.table_klassen.getY() - 20); // der einfachkeit halber =)
		label_klassen.setLocation(this.table_klassen.getX() + 20,
				this.table_klassen.getY() - 20);
		label_lehrer.setLocation(this.table_lehrer.getX() + 20,
				this.table_lehrer.getY() - 20);
		label_paedas.setLocation(this.table_paedas.getX() + 20,
				this.table_paedas.getY() - 20);

		/* Ende */

		table.setFillsViewportHeight(true);
		table.setGridColor(Color.YELLOW);
		table.setBackground(Color.CYAN);
		table.setRowHeight(ROW_HEIGHT);

		add(scrollPane);
		pack();

		this.setLocation(50, 50);
		this.setResizable(false);
		setVisible(true);

	}

	/**
	 * Ein Actionlistener, der daf�r sorgt, dass beim anklicken von addPaeda, addLehrer oder addKlasse 
	 * das jeweilige Fenster ge�ffnet wird.
	 * @author Fafo
	 *
	 */
	private class Handler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == addPaeda){
				final FramePaeda addFrame = new FramePaeda();
				addFrame.pack();
				addFrame.setVisible(true);
				addFrame.setSize(400, 300);
			} else if (event.getSource() == addLehrer) {
				final FrameLehrer addFrame = new FrameLehrer();
				addFrame.pack();
				addFrame.setVisible(true);
				addFrame.setSize(400, 300);
			} else if (event.getSource() == addKlasse) {
				final FrameClass addFrame = new FrameClass();
				addFrame.pack();
				addFrame.setVisible(true);
				addFrame.setSize(400, 300);
			}
		}
	}

	/**
	 * Diese Methode schlie�t unser Frame und �ffnet ein neues. Dies dient der Aktualisierung.
	 */
	public static void updateGUI(){
		Stundenplan.mainFrame.dispose();
		Stundenplan.mainFrame = new MainFrame();
		ErrorHandler.setMainFrame(Stundenplan.mainFrame);
		Stundenplan.mainFrame.pack();
		Stundenplan.mainFrame.setVisible(true);
		Stundenplan. mainFrame.setSize(900, 600);
	}
		
}
