package de.unibremen.swp.stundenplan.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.Aktivitaet;
import de.unibremen.swp.stundenplan.data.Paeda;
import de.unibremen.swp.stundenplan.data.Schoolclass;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.data.Timeslot;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.logic.TimetableManager;
import de.unibremen.swp.stundenplan.persistence.Data;

public class FrameAktivitaet extends JFrame {
	/**
	 *  Die eineindeutige ID f�r Serialisierung.
	 */
	private static final long serialVersionUID = -6549082687883264312L;

	/**
	 * Die Tabellen des Frames
	 */
	private final JTable table_klassen, table_lehrer, table_paedas;

	/**
	 * Die Bezeichnung der Aktivit�t
	 */
	private JTextField bezeichnung;
	
	/**
	 * Der Text �ber die Bezeichnung
	 */
	private JLabel jBezeichnung;

	/**
	 * Der Buttom zum best�tigen
	 */
	private JButton bestaetigen;

	/**
	 * Der Timeslot der gerade bearbeitet wird
	 */
	private Timeslot ts;
	
	/**
	 * Der Wochentag der bearbeitet wird
	 */
	private Weekday wk;
	
	/**
	 * Die Position des Timeslots in der Tabelle
	 */
	private int position;
	
	/**
	 * Erzeugt ein neues Frame f�r die Aktivit�t.
	 * Erstellt Tabellen mit allen Lehrern/P�dagogen/Schulklassen
	 * au�erdem wird ein Textfeld und ein Buttom zum best�tigen erstellt.
	 * @param fach
	 * 		Die Bezeichnung der Aktivit�t
	 */
	public FrameAktivitaet(final String fach){
		super();
		setTitle("Aktivitaet hinzuf�gen");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setResizable(false);
		setVisible(true);

		String[] col = { "Teacher" };
		Object[][] data = new Object[1][1];
		try {
			int size = 0;
			ArrayList<Teacher> teachers = new ArrayList<>();
			for(Teacher t : Data.getAllTeachers()){
				if(t.isEnable()){
					teachers.add(t);
					size++;
				}
			}
			data = new Object[size][1];
			int counter = 0;
			for (int i = 0; i < size; i++) {
				counter = 0;
				for (Teacher t : teachers){
					if (counter == i) {
						if(!t.isEnable()){
							break;
						}
						data[i][0] = t.getName() + " (" + t.getAcronym() + ")";
					}
					counter++;
				}
			}
		} catch (DatasetException e) {
			e.printStackTrace();
		}
		
		String[] colClass = { "Klasse" };
		Object[][] dataClass = new Object[1][1];
		try {
			int size = 0;
			ArrayList<Schoolclass> sClasses = new ArrayList<>();
			for(Schoolclass s : Data.getAllSchoolclasses()){
				if(s.isEnable()){
					sClasses.add(s);
					size++;
				}
			}
			dataClass = new Object[size][1];
			int counter = 0;
			for (int i = 0; i < size; i++) {
				counter = 0;
				for (Schoolclass s : sClasses){
					if (counter == i) {
						if(!s.isEnable()){
							break;
						}
						dataClass[i][0] = s.getName();
					}
					counter++;
				}
			}
		} catch (DatasetException e) {
			e.printStackTrace();
		}
		
		String[] colPeda = { "P�dagogen" };
		Object[][] dataPeda = new Object[1][1];
		try {
			int size = 0;
			ArrayList<Paeda> paedas = new ArrayList<>();
			for(Paeda p : Data.getAllPaeders()){
				if(p.isEnable()){
					paedas.add(p);
					size++;
				}
			}
			dataPeda = new Object[size][1];
			int counter = 0;
			for (int i = 0; i < size; i++) {
				counter = 0;
				for (Paeda p : paedas){
					if (counter == i) {
						if(!p.isEnable()){
							break;
						}
						dataPeda[i][0] = p.getName() + " (" + p.getAcronym() + ")";
					}
					counter++;
				}
			}
		} catch (DatasetException e) {
			e.printStackTrace();
		}
		
		TableModel modelTeacher = new DefaultTableModel(data, col);
		TableModel modelClass = new DefaultTableModel(dataClass, colClass);
		TableModel modelPeda = new DefaultTableModel(dataPeda, colPeda);
		 
		table_klassen = new JTable(modelClass){
				public boolean isCellEditable(int x, int y) {
	                return false;
	            }
	        };

		table_lehrer = new JTable(modelTeacher){
			public boolean isCellEditable(int x, int y) {
                return false;
            }
        };
        
        table_paedas = new JTable(modelPeda){
			public boolean isCellEditable(int x, int y) {
                return false;
            }
        };
		
		bestaetigen = new JButton("Best�tigen");
		if(fach == null){
			bezeichnung = new JTextField("");
		}else{
			bezeichnung = new JTextField(fach);
		}
		jBezeichnung = new JLabel("Die Bezeichnung der neuen Aktivitaet");
		
		add(bestaetigen);
		bestaetigen.setLocation(180,300);
		bestaetigen.setSize(MainFrame.BUTTONBREITE,MainFrame.BUTTONHOEHE);
		
		add(table_klassen);
		table_klassen.setLocation(20, 100);
		table_klassen.setSize(133, 190);

		add(table_lehrer);
		table_lehrer.setLocation(170, 100);
		table_lehrer.setSize(133, 190);

		add(table_paedas);
		table_paedas.setLocation(320, 100);
		table_paedas.setSize(133, 190);

		Handler handler = new Handler();

		bestaetigen.addActionListener(handler);

		bezeichnung.setSize(100, 20);
		jBezeichnung.setSize(250, 20);
		
		jBezeichnung.setLocation(140, 30);
		bezeichnung.setLocation(190, 60);
		
		add(bezeichnung);
		add(jBezeichnung);
		
		pack();
	}

	
	/**
	 * Setzt den Timeslot den man bearbeiten m�chte
	 * 
	 * @param pTimeslot
	 * 		Der Timeslot
	 * @param row
	 * 		Die Poision des Timeslot in der Tabelle
	 * @param weekday
	 * 		Der Wochentag
	 */
	public void setTimeslot(final Timeslot pTimeslot, final int row, final Weekday weekday){
		ts = pTimeslot;
		position = row;
		wk = weekday;
	}

	/**
	 * Ein eigener Maushorcher f�r de Button zum Best�tigen
	 */
	private class Handler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == bestaetigen) {
				Aktivitaet ak = new Aktivitaet();
				int[] klassen_selected = table_klassen.getSelectedRows();
				int[] lehrer_selected = table_lehrer.getSelectedRows();
				int[] paedas_selected = table_paedas.getSelectedRows();
				if(paedas_selected == null && lehrer_selected == null && klassen_selected == null){
					JOptionPane.showMessageDialog(null, "W�hle einen Lehrer, P�dagogen oder Klassse aus!");
					return;
				}
				try {
					ak.setBezeichnung(bezeichnung.getText());
					//Pr�fe
					for(int i: lehrer_selected){
						Object o = MainFrame.table_lehrer.getValueAt(i, 0);
						String[] split = o.toString().split("\\(");
						String acronym = split[1].substring(0, 3);
						Teacher teacher = Data.getTeacherByAcronym(acronym);
						Timeslot tsTeacher = teacher.getWeekTable().getDayTableforDay(wk).getTimeslot(position);
						Aktivitaet akTeach = tsTeacher.getAktivitaet();
						if(!teacher.hatNochZeit()){
							JOptionPane.showMessageDialog(null, "Der Lehrer �bersteigt seine maximalen Stunden pro Woche!");
							return;
						}
						if(akTeach != null){
							JOptionPane.showMessageDialog(null, "Der Lehrer " + teacher.getAcronym() + " arbeitet in diesen TimeSlot schon!");
							return;
						}
					}
					for(int i: paedas_selected){
						Object o = MainFrame.table_paedas.getValueAt(i, 0);
						String[] split = o.toString().split("\\(");
						String acronym = split[1].substring(0, 3);
						Paeda paeda = Data.getPaedaByAcronym(acronym);
						Timeslot tspaeda = paeda.getWeekTable().getDayTableforDay(wk).getTimeslot(position);
						Aktivitaet akpaeda = tspaeda.getAktivitaet();
						if(!paeda.hatNochZeit()){
							JOptionPane.showMessageDialog(null, "Der P�dagoge �bersteigt seine maximalen Stunden pro Woche!");
							return;
						}
						if(akpaeda != null){
							JOptionPane.showMessageDialog(null, "Der P�dagoge " + paeda.getAcronym() + " arbeitet in diesen TimeSlot schon!");
							return;
						}
					}
					for(int i: klassen_selected){
						Schoolclass sClass = Data.getSchoolclassByName((table_klassen.getValueAt(i, 0).toString()));
						Timeslot tsClass = sClass.getWeekTable().getDayTableforDay(wk).getTimeslot(position);
						Aktivitaet akClass = tsClass.getAktivitaet();
						if(akClass != null){
							JOptionPane.showMessageDialog(null, "Die Klasse " + sClass.getName() + " hat dort schon Unterricht!");
							return;
						}
					}
					//Ende Pr�fung
					for(int i: lehrer_selected){
						Object o = MainFrame.table_lehrer.getValueAt(i, 0);
						String[] split = o.toString().split("\\(");
						String acronym = split[1].substring(0, 3);
						Teacher teacher = Data.getTeacherByAcronym(acronym);
						Timeslot tsTeacher = teacher.getWeekTable().getDayTableforDay(wk).getTimeslot(position);
						Aktivitaet akTeach = tsTeacher.getAktivitaet();
						akTeach = new Aktivitaet();
						upadteAktivitaet(ak, teacher, null, null);
						teacher.currentHoursPerWeekPlusEins();
						tsTeacher.setAktivitaet(akTeach);
						Data.updateAktivitaet(akTeach);
						Data.updateTimeslot(tsTeacher);
						Data.updateTeacher(teacher);
						ak.addTeacher(teacher);
					}
					for(int i: paedas_selected){
						Object o = MainFrame.table_paedas.getValueAt(i, 0);
						String[] split = o.toString().split("\\(");
						String acronym = split[1].substring(0, 3);
						Paeda paeda = Data.getPaedaByAcronym(acronym);
						Timeslot tspaeda = paeda.getWeekTable().getDayTableforDay(wk).getTimeslot(position);
						Aktivitaet akpaeda = tspaeda.getAktivitaet();
						akpaeda = new Aktivitaet();
						upadteAktivitaet(akpaeda, null, paeda, null);
						paeda.currentHoursPerWeekPlusEins();
						tspaeda.setAktivitaet(akpaeda);
						Data.updateAktivitaet(akpaeda);
						Data.updateTimeslot(tspaeda);
						Data.updatePaeda(paeda);
						ak.addPaeda(paeda);
					}
					for(int i: klassen_selected){
						Schoolclass sClass = Data.getSchoolclassByName((table_klassen.getValueAt(i, 0).toString()));
						Timeslot tsClass = sClass.getWeekTable().getDayTableforDay(wk).getTimeslot(position);
						Aktivitaet akClass = tsClass.getAktivitaet();
						akClass = new Aktivitaet();
						upadteAktivitaet(akClass, null, null, sClass);
						tsClass.setAktivitaet(akClass);
						Data.updateAktivitaet(akClass);
						Data.updateTimeslot(tsClass);
						Data.updateSchoolclass(sClass);
						ak.addClass(sClass);
					}
					ts.setAktivitaet(ak);
					Data.addAktivitaet(ak);
					Data.updateTimeslot(ts);
					setTimeslot(wk, position);
					dispose();
				} catch (DatasetException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		}
	}
	
	/**
	 * Setzt den Timeslot in die Tabelle
	 * @param weekday
	 * 		Der Wochentag
	 * @param position
	 * 		Die Poision des Timeslot in der Tabelle
	 */
	public void setTimeslot(final Weekday weekday, final int position) {
		try {
			TimetableManager.getTimeslotAt(weekday, position);
			pack();
		} catch (DatasetException e) {
		}
	}
	
	/**
	 * Update eine Aktivit�t von jemand anderen.
	 */
	public void upadteAktivitaet(final Aktivitaet ak, final Teacher t, final Paeda p, final Schoolclass s){
		int[] klassen_selected = table_klassen.getSelectedRows();
		int[] lehrer_selected = table_lehrer.getSelectedRows();
		int[] paedas_selected = table_paedas.getSelectedRows();
		try {
			ak.setBezeichnung(bezeichnung.getText());
			for(int i: klassen_selected){
				Schoolclass sClass = Data.getSchoolclassByName((table_klassen.getValueAt(i, 0).toString()));
				if(sClass != s && sClass != null)ak.addClass(sClass);
			}
			for(int i: lehrer_selected){
				Object o = MainFrame.table_lehrer.getValueAt(i, 0);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				Teacher teacher = Data.getTeacherByAcronym(acronym);
				if(teacher != t && teacher != null)ak.addTeacher(teacher);
			}
			for(int i: paedas_selected){
				Object o = MainFrame.table_paedas.getValueAt(i, 0);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				Paeda paeda = Data.getPaedaByAcronym(acronym);
				if(paeda != p && paeda != null)ak.addPaeda(paeda);
			}
			String aco = MainFrame.bezeichnung.getText();
			if(aco.length() == 2){
				Schoolclass sClass = Data.getSchoolclassByName(aco);
				ak.addClass(sClass);
			}else if(Data.getTeacherByAcronym(aco) != null){
				Teacher teacher = Data.getTeacherByAcronym(aco);
				ak.addTeacher(teacher);
			}else if(Data.getPaedaByAcronym(aco) != null){
				Paeda paeda = Data.getPaedaByAcronym(aco);
				ak.addPaeda(paeda);
			}
		}catch(DatasetException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
}

