package de.unibremen.swp.stundenplan.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;

import de.unibremen.swp.stundenplan.config.Messages;
import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.data.Timeslot;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.logic.TeacherManager;
import de.unibremen.swp.stundenplan.logic.TimetableManager;

/**
 * 
 * @author Kai
 *
 */
public class addAktivitaetDialog extends JDialog implements
		PropertyChangeListener, ListSelectionListener {
	/**
	 * Logger dieser Klasse zum Protokollieren von Ereignissen und
	 * Informationen.
	 */
	private static final Logger LOGGER = Logger
			.getLogger(TimetableRenderer.class.getName());

	/**
	 * Die eindeutige Kennzeichnung f�r Serialisierung.
	 */
	private static final long serialVersionUID = 8990701412087003806L;

	/**
	 * String-Konstante f�r die OK-Nachricht. Verhindert unn�tige
	 * Mehrfachinstanziierung, da die Nachricht mehrfach verwendet wird.
	 */
	private static final String MSG_OK = Messages
			.getString("AddAktivitaetDialog.OK");

	/**
	 * Der aktuelle Timeslot.
	 */
	private Timeslot timeslot;

	/**
	 * Eine JOptionPane.
	 */
	private final JOptionPane contentPane;

	/**
	 * Der Konstruktor. Setzt die Darstellungseigenschaften des Dialoges.
	 * 
	 * @param owner
	 *            Der Frame, von dem der Dialog aus aufgerufen wird.
	 */
	public addAktivitaetDialog(final JFrame owner) {
		super(owner, Messages.getString("AddAktivitaetDialog.AddAktivitaet"),
				true);
		final JLabel label = new JLabel(
				Messages.getString("AddAktivitaetDialog.AddAktivitaet"));

		final Object[] elements = { label };
		final String[] buttonLabels = { MSG_OK,
				Messages.getString("AddTeacherDialog.Cancel") };

		contentPane = new JOptionPane(elements, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.OK_CANCEL_OPTION, null, buttonLabels,
				buttonLabels[1]);
		setContentPane(contentPane);
		final JPanel buttonPanel = (JPanel) contentPane.getComponent(1);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(final WindowEvent windowEvent) {
				contentPane
						.setValue(Integer.valueOf(JOptionPane.CLOSED_OPTION));
			}
		});

		contentPane.addPropertyChangeListener(this);
	}

	/**
	 * Setzt den Timeslot auf die Zeiteinheit die sich aus dem gegebenen
	 * Wochentag und der gegebenen Position ergibt.
	 * 
	 * @param weekday
	 *            Der Wochentag der Zeiteinheit
	 * @param position
	 *            die Position der Zeiteinheit
	 */
	public void setTimeslot(final Weekday weekday, final int position) {
		try {
			timeslot = TimetableManager.getTimeslotAt(weekday, position);
			pack();

		} catch (DatasetException e) {
			LOGGER.error("Exception while setting timeslot " + timeslot, e);
			ErrorHandler.criticalDatasetError();
		}

	}

	
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		final String prop = event.getPropertyName();

		if (isVisible()
				&& event.getSource() == contentPane
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			final Object value = contentPane.getValue();

			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				return;
			}

			contentPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (value.equals(MSG_OK)) {
				updateTimeslot();
			}
			clearAndHide();
		}
	}

	/**
	 * Aktualisiert den Timeslot gemaess der Auswahl in der Popup-Liste.
	 */
	private void updateTimeslot() {
		try {
			TimetableManager.updateTimeslot(timeslot);
		} catch (DatasetException ex) {
			LOGGER.error("Exception while updating timeslot " + timeslot, ex);
			ErrorHandler.criticalDatasetError();
		}
	}

	/**
	 * Setzt den Dialog auf unsichtbar.
	 */
	public void clearAndHide() {
		setVisible(false);
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		
		
	}
}
