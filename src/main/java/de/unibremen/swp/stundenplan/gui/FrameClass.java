package de.unibremen.swp.stundenplan.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import de.unibremen.swp.stundenplan.Stundenplan;
import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.DayTable;
import de.unibremen.swp.stundenplan.data.Schoolclass;
import de.unibremen.swp.stundenplan.data.WeekTable;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.logic.TimetableManager;
import de.unibremen.swp.stundenplan.persistence.Data;

public class FrameClass extends JFrame {

	/**
	 * Die eindeutige Kennzeichnung f�r Serialisierung.
	 */
	private static final long serialVersionUID = -8108606477993299983L;

	/**
	 * Ein  JTextField. 
	 */
	private JTextField name;
	
	/**
	 * Ein JLabel fuer den Namen der Klasse.
	 */
	private JLabel jName;
	
	/**
	 * Der Button zum best�tigen.
	 */
	private JButton bestaetigen;
	
	/**
	 * Der Konstruktor. Setzt die Komponenten und f�gt den Button einem Actionhandler hinzu. 
	 */
	public FrameClass() {
		super();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Schulklasse hinzuf�gen");
		setResizable(false);
		setVisible(true);
		
		bestaetigen = new JButton("Best�tigen");
		name = new JTextField("");
		jName = new JLabel("Der Name der Klasse");
		
		name.setSize(100, 20);
		jName.setSize(200,20);
		bestaetigen.setSize(MainFrame.BUTTONBREITE,MainFrame.BUTTONHOEHE);
		
		name.setLocation(150, 60);
		jName.setLocation(120, 30);
		bestaetigen.setLocation(125, 190);
		
		Handler handler = new Handler();
		
		bestaetigen.addActionListener(handler);
		
		add(name);
		add(bestaetigen);
		add(jName);
		
		pack();

	}
	
	/**
	 * Der Actionlistener fuer den Button.
	 */
	
	private class Handler implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent event) {	
			if(event.getSource()==bestaetigen){
				Schoolclass sClass = new Schoolclass();
				String pName = name.getText();
				try{
					if(Data.getSchoolclassByName(pName) != null){
						JOptionPane.showMessageDialog(null, "Eine Schulklasse mit dem Namen gibt es schon!");
						return;
					}
					sClass.setName(pName);
					WeekTable wt = new WeekTable();
					for (final Weekday weekday : Weekday.values()) {
						final DayTable dayTable = TimetableManager.createDayTable(weekday);
						wt.addDayTable(dayTable);
					}
					Data.addWeekTable(wt);
					sClass.setWeekTable(wt);
					sClass.setEnable(true);
					Data.addSchoolclass(sClass);
					setVisible(false);
					dispose();
					MainFrame.updateGUI();
					JOptionPane.showMessageDialog(null, "Schulklasse erfolgreich erstellt");
				}catch(IllegalArgumentException  e){
					JOptionPane.showMessageDialog(null, e.getMessage());
				}catch(DatasetException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				 
			}	
		}
	}
	
}
