package de.unibremen.swp.stundenplan.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

import de.unibremen.swp.stundenplan.config.Weekday;
import de.unibremen.swp.stundenplan.data.Aktivitaet;
import de.unibremen.swp.stundenplan.data.Paeda;
import de.unibremen.swp.stundenplan.data.Schoolclass;
import de.unibremen.swp.stundenplan.data.Teacher;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.persistence.Data;

/**
 * Rep�sentiert unterschiedliche Mauslistener f�r die Tabellen im MainFraim
 * 
 * @author Marcel
 */
public class MouseListeners {
	
	/**
	 * Der Mauslistener f�r Die Paeda-Tabelle
	 */
	private MyMousePaeda mousePeada;
	
	/**
	 * Der Mauslistener f�r Die Schoolclass-Tabelle
	 */
	private MyMouseClass mouseClass;
	
	/**
	 * Der Mauslistener f�r Die Teacher-Tabelle
	 */
	private MyMouseTeacher mouseTeacher;
	
	/**
	 * Erzeugt einen MausListener mit den zugeh�rigen Listeners
	 */
	public MouseListeners(){
		mousePeada = new MyMousePaeda();
		mouseClass = new MyMouseClass();
		mouseTeacher = new MyMouseTeacher();
	}
	
	/**
	 * @return	Der Listener f�r die Tabelle Paeda
	 */
	public MyMousePaeda getPaedaMouse(){
		return mousePeada;
	}
	
	/**
	 * @return	Der Listener f�r die Tabelle Schoolclass
	 */
	public MyMouseClass getClassMouse(){
		return mouseClass;
	}
	
	/**
	 * @return	Der Listener f�r die Tabelle Teacher
	 */
	public MyMouseTeacher getTeacherMouse(){
		return mouseTeacher;
	}

	/**
	 * Ein eigener Maushorcher f�r die Tabelle Paeda
	 */
	public class MyMousePaeda extends MouseAdapter {
		@Override
		public void mousePressed(final MouseEvent event) {
			final int row = MainFrame.table_paedas.rowAtPoint(event.getPoint());
			final int col = MainFrame.table_paedas.columnAtPoint(event.getPoint());
			if (row >= 0 && row < MainFrame.table_paedas.getRowCount() && col >= 0
					&& col < MainFrame.table_paedas.getColumnCount()) {
				MainFrame.table_paedas.changeSelection(row, col, false, false);
			} else {
				MainFrame.table_paedas.clearSelection();
			}
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
			event.consume();
		}

		@Override
		public void mouseReleased(final MouseEvent event) {
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Prueft, ob es sich bei dem gegebenen Mausereignis um einen
		 * Rechtsklick handelt. Falls das so ist, wird ein entsprechendes
		 * Popup-Menu an den durch das Mausereignis uebermittelten Koordinaten
		 * geoeffnet.
		 * 
		 * Vermeidet Redundanz in den Listenern fuer mouse-pressed und
		 * mouse-released-Ereignisse. Beide Listener sind noetig, da Windoof den
		 * Popup-Trigger erst auf Loslassen der Maustaste meldet, Linux und
		 * MacOs aber bereits beim Klicken der Maus.
		 * 
		 * @param event
		 *            das zu pruefende Mausevent
		 * @throws DatasetException
		 */
		private void checkPopup(final MouseEvent event) throws DatasetException {
			final int row = MainFrame.table_paedas.rowAtPoint(event.getPoint());
			final int col = MainFrame.table_paedas.columnAtPoint(event.getPoint());
			if (event.isPopupTrigger()) {
				/*
				 * Verhindert den nochmaligen Aufruf unter Linux und MacOs.
				 */
				event.consume();
				if (event.getComponent() instanceof JTable && row >= 0
						&& col >= 0) {
					final JPopupMenu popup = createPopupPaeda(row, col);
					popup.show(MainFrame.table_paedas, event.getX(), event.getY());
				}
			}
		}
	}
	
	/**
	 * Erzeugt ein Popup zum L�schen/Editieren/Zeige Stundenplan eines P�dagogen
	 * 
	 * @param row
	 *            Die Zeile.
	 * @param col
	 *            Die Spalte.
	 * @return das neue Popup-Menu
	 * @throws DatasetException
	 */
	private JPopupMenu createPopupPaeda(final int row, final int col)throws DatasetException {
		final JPopupMenu popmen = new JPopupMenu();
		final JMenuItem menu1 = new JMenuItem("L�sche P�dagoge");
		final JMenuItem menu2 = new JMenuItem("Editiere P�dagoge");
		final JMenuItem menu3 = new JMenuItem("Zeige Stundenplan");
		menu1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_paedas.getValueAt(row, col);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				Paeda paeda = null;
				if (event.getSource() == menu1){
					try {
						paeda = Data.getPaedaByAcronym(acronym);
						paeda.setEnable(false);
						Data.updatePaeda(paeda);
						MainFrame.bezeichnung = null;
						for(Schoolclass sc : Data.getAllSchoolclasses()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = sc.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getPaedaByaAcronym(acronym) != null){
											ak.removePaeda(paeda);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						for(Teacher teacher : Data.getAllTeachers()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = teacher.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getPaedaByaAcronym(acronym) != null){
											ak.removePaeda(paeda);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						for(Paeda pPaeda : Data.getAllPaeders()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = pPaeda.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getPaedaByaAcronym(acronym) != null){
											ak.removePaeda(paeda);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						MainFrame.updateGUI();
						JOptionPane.showMessageDialog(null, "P�dagoge erfolgreich gel�scht");
					} catch (DatasetException e) {
						JOptionPane.showMessageDialog(null, "P�dagoge konnte nicht gel�scht werden");
					}
				}
			}
		});
		menu2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_paedas.getValueAt(row, col);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				Paeda paeda = null;
				if(event.getSource() == menu2){
					try {
						paeda = Data.getPaedaByAcronym(acronym);
					} catch (DatasetException e) {
						JOptionPane.showMessageDialog(null, "P�dagoge konnte nicht bearbeitet werden");
						return;
					}
					final FrameEditPaeda editPaeda = new FrameEditPaeda(paeda);
					editPaeda.pack();
					editPaeda.setVisible(true);
					editPaeda.setSize(400, 300);
				}
			}
		});
		menu3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_paedas.getValueAt(row, col);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				if(event.getSource() == menu3){
					MainFrame.bezeichnung = new JLabel(acronym);
					MainFrame.updateGUI();
				}
			}
		});
		popmen.add(menu1);
		popmen.add(menu2);
		popmen.add(menu3);
		popmen.setVisible(true);
		return popmen;
	}
	
	
	/**
	 * Ein eigener Maushorcher f�r die Tabelle Schoolclass
	 */
	public class MyMouseClass extends MouseAdapter {
		@Override
		public void mousePressed(final MouseEvent event) {
			final int row = MainFrame.table_klassen.rowAtPoint(event.getPoint());
			final int col = MainFrame.table_klassen.columnAtPoint(event.getPoint());
			if (row >= 0 && row < MainFrame.table_klassen.getRowCount() && col >= 0
					&& col < MainFrame.table_klassen.getColumnCount()) {
				MainFrame.table_klassen.changeSelection(row, col, false, false);
			} else {
				MainFrame.table_klassen.clearSelection();
			}
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
			event.consume();
		}

		@Override
		public void mouseReleased(final MouseEvent event) {
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Prueft, ob es sich bei dem gegebenen Mausereignis um einen
		 * Rechtsklick handelt. Falls das so ist, wird ein entsprechendes
		 * Popup-Menu an den durch das Mausereignis uebermittelten Koordinaten
		 * geoeffnet.
		 * 
		 * Vermeidet Redundanz in den Listenern fuer mouse-pressed und
		 * mouse-released-Ereignisse. Beide Listener sind noetig, da Windoof den
		 * Popup-Trigger erst auf Loslassen der Maustaste meldet, Linux und
		 * MacOs aber bereits beim Klicken der Maus.
		 * 
		 * @param event
		 *            das zu pruefende Mausevent
		 * @throws DatasetException
		 */
		private void checkPopup(final MouseEvent event) throws DatasetException {
			final int row = MainFrame.table_klassen.rowAtPoint(event.getPoint());
			final int col = MainFrame.table_klassen.columnAtPoint(event.getPoint());
			if (event.isPopupTrigger()) {
				/*
				 * Verhindert den nochmaligen Aufruf unter Linux und MacOs.
				 */
				event.consume();
				if (event.getComponent() instanceof JTable && row >= 0
						&& col >= 0) {
					final JPopupMenu popup = createPopupClass(row, col);
					popup.show(MainFrame.table_klassen, event.getX(), event.getY());
				}
			}
		}
	}
	
	/**
	 * Erzeugt ein Popup zum L�schen/Zeige Stundenplan einer Schulklasse
	 * 
	 * @param row
	 *            Die Zeile.
	 * @param col
	 *            Die Spalte.
	 * @return das neue Popup-Menu
	 * @throws DatasetException
	 */
	private JPopupMenu createPopupClass(final int row, final int col)throws DatasetException {
		final JPopupMenu popmen = new JPopupMenu();
		final JMenuItem menu1 = new JMenuItem("L�sche Schulklasse");
		final JMenuItem menu3 = new JMenuItem("Zeige Stundenplan");
		menu1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_klassen.getValueAt(row, col);
				String name = o.toString();
				Schoolclass sClass = null;
				if (event.getSource() == menu1){
					try {
						sClass = Data.getSchoolclassByName(name);
						sClass.setEnable(false);
						Data.updateSchoolclass(sClass);
						MainFrame.bezeichnung = null;
						for(Teacher teacher : Data.getAllTeachers()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = teacher.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getKlasseByName(name) != null){
											ak.removeClass(sClass);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						for(Paeda paeda : Data.getAllPaeders()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = paeda.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getKlasseByName(name) != null){
											ak.removeClass(sClass);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						MainFrame.updateGUI();
						JOptionPane.showMessageDialog(null, "Schulklasse erfolgreich gel�scht");
					} catch (DatasetException e) {
						JOptionPane.showMessageDialog(null, "Schuklasse konnte nicht gel�scht werden");
					}
				}
			}
		});
		menu3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_klassen.getValueAt(row, col);
				String name = o.toString();
				if(event.getSource() == menu3){
					MainFrame.bezeichnung = new JLabel(name);
					MainFrame.updateGUI();
				}
			}
		});
		popmen.add(menu1);
		popmen.add(menu3);
		popmen.setVisible(true);
		return popmen;
	}
	
	/**
	 * Ein eigener Maushorcher f�r die Tabelle Lehrer
	 */
	public class MyMouseTeacher extends MouseAdapter {
		@Override
		public void mousePressed(final MouseEvent event) {
			final int row = MainFrame.table_lehrer.rowAtPoint(event.getPoint());
			final int col = MainFrame.table_lehrer.columnAtPoint(event.getPoint());
			if (row >= 0 && row < MainFrame.table_lehrer.getRowCount() && col >= 0
					&& col < MainFrame.table_lehrer.getColumnCount()) {
				MainFrame.table_lehrer.changeSelection(row, col, false, false);
			} else {
				MainFrame.table_lehrer.clearSelection();
			}
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
			event.consume();
		}

		@Override
		public void mouseReleased(final MouseEvent event) {
			try {
				checkPopup(event);
			} catch (DatasetException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Prueft, ob es sich bei dem gegebenen Mausereignis um einen
		 * Rechtsklick handelt. Falls das so ist, wird ein entsprechendes
		 * Popup-Menu an den durch das Mausereignis uebermittelten Koordinaten
		 * geoeffnet.
		 * 
		 * Vermeidet Redundanz in den Listenern fuer mouse-pressed und
		 * mouse-released-Ereignisse. Beide Listener sind noetig, da Windoof den
		 * Popup-Trigger erst auf Loslassen der Maustaste meldet, Linux und
		 * MacOs aber bereits beim Klicken der Maus.
		 * 
		 * @param event
		 *            das zu pruefende Mausevent
		 * @throws DatasetException
		 */
		private void checkPopup(final MouseEvent event) throws DatasetException {
			final int row = MainFrame.table_lehrer.rowAtPoint(event.getPoint());
			final int col = MainFrame.table_lehrer.columnAtPoint(event.getPoint());
			if (event.isPopupTrigger()) {
				/*
				 * Verhindert den nochmaligen Aufruf unter Linux und MacOs.
				 */
				event.consume();
				if (event.getComponent() instanceof JTable && row >= 0
						&& col >= 0) {
					final JPopupMenu popup = createPopupTeacher(row, col);
					popup.show(MainFrame.table_lehrer, event.getX(), event.getY());
				}
			}
		}
	}
	
	/**
	 * Erzeugt ein Popup zum L�schen/Editieren/Zeige Stundenplan eines Lehrers
	 * 
	 * @param row
	 *            Die Zeile.
	 * @param col
	 *            Die Spalte.
	 * @return das neue Popup-Menu
	 * @throws DatasetException
	 */
	private JPopupMenu createPopupTeacher(final int row, final int col)throws DatasetException {
		final JPopupMenu popmen = new JPopupMenu();
		final JMenuItem menu1 = new JMenuItem("L�sche Lehrer");
		final JMenuItem menu2 = new JMenuItem("Editiere Lehrer");
		final JMenuItem menu3 = new JMenuItem("Zeige Stundenplan");
		menu1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_lehrer.getValueAt(row, col);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				Teacher teacher = null;
				if (event.getSource() == menu1){
					try {
						teacher = Data.getTeacherByAcronym(acronym);
						teacher.setEnable(false);
						Data.updateTeacher(teacher);
						MainFrame.bezeichnung = null;
						for(Paeda paeda : Data.getAllPaeders()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = paeda.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getTeacherByaAcronym(acronym) != null){
											ak.removeTeacher(teacher);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						for(Schoolclass sc : Data.getAllSchoolclasses()){
							for(Weekday wd : Weekday.values()){
								for(int i = 0; i < 8; i++){
									Aktivitaet ak = sc.getWeekTable().getDayTableforDay(wd).getTimeslot(i).getAktivitaet();
									if(ak != null){
										if(ak.getTeacherByaAcronym(acronym) != null){
											ak.removeTeacher(teacher);
											Data.updateAktivitaet(ak);
										}
									}
								}
							}
						}
						MainFrame.updateGUI();
						JOptionPane.showMessageDialog(null, "Lehrer erfolgreich gel�scht");
					} catch (DatasetException e) {
						JOptionPane.showMessageDialog(null, "Lehrer konnte nicht gel�scht werden");
					}
				}
			}
		});
		menu2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_lehrer.getValueAt(row, col);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				Teacher teacher = null;
				if(event.getSource() == menu2){
					try {
						teacher = Data.getTeacherByAcronym(acronym);
					} catch (DatasetException e) {
						JOptionPane.showMessageDialog(null, "Lehrer konnte nicht bearbeitet werden");
						return;
					}
					final FrameEditTeacher editTeacher = new FrameEditTeacher(teacher);
					editTeacher.pack();
					editTeacher.setVisible(true);
					editTeacher.setSize(400, 300);
				}
			}
		});
		menu3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				Object o = MainFrame.table_lehrer.getValueAt(row, col);
				String[] split = o.toString().split("\\(");
				String acronym = split[1].substring(0, 3);
				if(event.getSource() == menu3){
					MainFrame.bezeichnung = new JLabel(acronym);
					MainFrame.updateGUI();
				}
			}
		});
		popmen.add(menu1);
		popmen.add(menu2);
		popmen.add(menu3);
		popmen.setVisible(true);
		return popmen;
	}
	
}
