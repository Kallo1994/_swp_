package de.unibremen.swp.stundenplan.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import de.unibremen.swp.stundenplan.data.Paeda;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.persistence.Data;

public class FrameEditPaeda extends JFrame {

	
	
	/**
	 * Die eineindeutige Kennzeichnung f�r Serialisierung.
	 */
	private static final long serialVersionUID = -6549082687883264302L;

	/**
	 * Dient dazu das der Benutzer die Dtaen eingeben kann
	 */
	private JTextField acronym, name, hoursperweek;

	/**
	 * Zeigt informnationen an, was der benutzer bei den text fields eingeben
	 * soll.
	 */
	private JLabel jName, jAcronym, jHoursperweek;

	/**
	 * Zum best�tigen der eingaben durch den benutzer
	 */
	private JButton bestaetigen;
	
	/**
	 * der paeda der editiert wird
	 */
	private Paeda paeda;
	
	/**
	 * Erstellt ein neues Fenster um den gegebenen paedagogischen mitarbeiter zu editieren
	 * @param pPaeda
	 */
	public FrameEditPaeda(final Paeda pPaeda) {
		super();
		paeda = pPaeda;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Padagogischen Mitarbeiter bearbeiten");
		setResizable(false);
		setVisible(true);
		
		bestaetigen = new JButton("Best�tigen");
		name = new JTextField(paeda.getName());
		acronym = new JTextField(paeda.getAcronym());
		hoursperweek = new JTextField(String.valueOf(paeda.getHoursPerWeek()));
		jName = new JLabel("Der Name des P�dagogen");
		jAcronym = new JLabel("Das Acronym des P�dagogen");
		jHoursperweek = new JLabel("Die Wochenstundezahl");
		
		name.setSize(100, 20);
		jName.setSize(200,20);
		acronym.setSize(100, 20);
		jAcronym.setSize(200,20);
		hoursperweek.setSize(100, 20);
		jHoursperweek.setSize(200,20);
		bestaetigen.setSize(MainFrame.BUTTONBREITE,MainFrame.BUTTONHOEHE);
		
		jName.setLocation(120, 30);
		name.setLocation(150, 60);
		
		jAcronym.setLocation(120, 80);
		acronym.setLocation(150, 110);
		
		jHoursperweek.setLocation(130, 130);
		hoursperweek.setLocation(150, 160);
		
		bestaetigen.setLocation(125, 190);
		
		Handler handler = new Handler();
		
		bestaetigen.addActionListener(handler);
		
		add(name);
		add(acronym);	
		add(hoursperweek);
		add(bestaetigen);
		add(jAcronym);
		add(jHoursperweek);
		add(jName);
		
		pack();

	}
	
	private class Handler implements ActionListener{
		/**
		 * Liest die daten des Benutzers aus und nimmt die �nderungen in der Db vor
		 */
		@Override
		public void actionPerformed(ActionEvent event) {	
			if(event.getSource()==bestaetigen){
				String pName = name.getText();
				String pAcronym = acronym.getText();
				String pHoursperweek = hoursperweek.getText();
				try{
					if(!pAcronym.equals(paeda.getAcronym())){
						JOptionPane.showMessageDialog(null, "Das K�rzel darf nicht ge�ndert werden.");
						return;
					}else{
						paeda.setName(pName);
						paeda.setAcronym(pAcronym);
						paeda.setHoursPerWeek(pHoursperweek);
						Data.updatePaeda(paeda);
					}
					setVisible(false);
					dispose();
					MainFrame.updateGUI();
					JOptionPane.showMessageDialog(null, "P�dagoge erfolgreich editiert");
				}catch(IllegalArgumentException  e){
					JOptionPane.showMessageDialog(null, e.getMessage());
				}catch(DatasetException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				 
			}	
		}
	}
	
}
