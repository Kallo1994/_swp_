package de.unibremen.swp.stundenplan;

import java.io.IOException;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import de.unibremen.swp.stundenplan.config.Config;
import de.unibremen.swp.stundenplan.exceptions.DatasetException;
import de.unibremen.swp.stundenplan.gui.ErrorHandler;
import de.unibremen.swp.stundenplan.gui.MainFrame;
import de.unibremen.swp.stundenplan.logic.TeacherManager;
import de.unibremen.swp.stundenplan.logic.TimetableManager;


/**
 * Hauptklasse f�r den Stundenplan mit main-Methode. Erzeugt eine Konfiguration und initialisiert die Logik-Komponenten
 * und die GUI und zeigt dann das Hauptfenster an.
 * 
 * @author D. L�demann, K. H�lscher
 * @version 0.1
 */
public final class Stundenplan {

    /**
     * Der Logger dieser Klasse.
     */
    private static final Logger LOGGER = Logger.getLogger(Stundenplan.class.getName());

    /**
     * Privater Konstruktor, der eine Instanziierung dieser Utility-Klasse verhindert.
     */
    private Stundenplan() {
    }
    
    /**
     * Das Hauptfenster unseres Stundenplanes
     */
    public static MainFrame mainFrame = null;

    /**
     * Startet die Anwendung. Erzeugt dazu einen neuen Stundenplaner und dann das Hauptanzeigefenster und macht dieses
     * sichtbar. Der Pfad zur Konfigurationsdatei kann als Parameter �bergeben werden.
     * 
     * Falls die Konfiguration nicht erzeugt werden kann, wird eine {@link IllegalStateException} ausgel�st.
     * 
     * @param args
     *            als erstes Argument kann der Pfad zur Konfigurationsdatei angegeben werden
     */
    public static void main(final String[] args) {
        LOGGER.info("Initialize configuration");
        try {
            if (args.length > 0) {
                Config.init(args[0]);
            } else {
                Config.init(null);
            }
        } catch (IOException e) {
            LOGGER.error("Exception while initializing configuration!", e);
            throw new IllegalStateException("Could not create configuration." + e.getMessage());
        }
        try {
            TeacherManager.init();
            TimetableManager.init();
        } catch (DatasetException e) {
            LOGGER.error("Exception while initializing logic!", e);
            throw new IllegalStateException("Could not initialize logic components: " + e.getMessage());
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                mainFrame = new MainFrame();
                ErrorHandler.setMainFrame(mainFrame);
                mainFrame.pack();
                mainFrame.setVisible(true);
                mainFrame.setSize(900, 600);
            }
        });

    }

}
