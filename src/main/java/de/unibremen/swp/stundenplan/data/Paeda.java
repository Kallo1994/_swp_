package de.unibremen.swp.stundenplan.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import de.unibremen.swp.stundenplan.persistence.Data;

/**
 * Repr�sentiert einen P�dagogischen MitarbeiterIn. Eine P�dagogische MitarbeiterIn hat einen Namen, ein systemweit eindeutiges Kuerzel und eine Anzahl von
 * Stunden, die sie unterrichtet bzw. arbeitet.
 * 
 * Zwei Paedagogische MitarbeiterInnen werden als gleich angesehen, wenn sie das gleiche Kuerzel haben.
 * 
 * @author M. Schwenk
 * @version 0.1
 */
@Entity
public final class Paeda implements Serializable {

    /**
     * Die generierte serialVersionUID.
     */
    private static final long serialVersionUID = -2391687797016927732L;

    /**
     * Der Wochenplan des paedagogischen Mitarbeiters
     */
    @OneToOne
    private WeekTable weekTable ;
    
    /**
     * Die minimale Zahl an Unterrichts- bzw. Arbeitsstunden, die jede Paedagogische MitarbeiterIn leisten muss. Kann nicht unterschritten
     * werden.
     */
    @Transient
    public static final int MIN_HOURS_PER_WEEK = 1;

    /**
     * Die maximale Zahl an Unterrichts- bzw. Arbeitsstunden, die jede Paedagogische MitarbeiterIn leisten muss. Kann nicht ueberschritten
     * werden.
     */
    @Transient
    public static final int MAX_HOURS_PER_WEEK = 40;
    
    /**
     * Die aktuelle Stundenzahl die diese Person pro Woche arbeitet.
     */
    private int currentHoursPerWeek;

    /**
     * Der Name dieser Paedagogischen MitarbeiterIn.
     */
    @Column(nullable = false, length = Data.MAX_NORMAL_STRING_LEN)
    private String name;

    /**
     * Das K�rzel dieser Paedagogischen MitarbeiterIn. Ein Kuerzel muss systemweit eindeutig sein.
     */
    @Id
    @Column(length = Data.MAX_ACRONYM_LEN)
    private String acronym;
    
    /**
     * Wenn enable auf "true", gilt der P�dagoge als Sichtbar.
     * Auf "false" gilt der P�dagoge als gel�scht und unsichtbar.
     */
    private boolean enable;

    /**
     * Die Anzahl an Stunden, die eine Paedagogische MitarbeiterIn maximal unterrichtet bzw. arbeitet. 
     */
    @Column
    private int hoursPerWeek;

    /**
     * Setzt den Namen dieser Paedagogischen MitarbeiterIn auf den Uebergebenen Namen. Falls der Name laenger als
     * {@linkplain Data#MAX_NORMAL_STRING_LEN} Zeichen ist, wird er entsprechend gekuerzt. Fuehrende und folgende
     * Leerzeichen werden entfernt. Laest eine {@link IllegalArgumentException} aus, falls der Name leer ist.
     * 
     * @param pName
     *            der neue Name dieser Paedagogischen MitarbeiterIn
     */
    public void setName(final String pName) {
        if (pName == null || pName.trim().isEmpty()) {
            throw new IllegalArgumentException("Der Name der P�dagogischen MitarbeiterIn ist leer");
        }
        name = pName.trim().substring(0, Math.min(Data.MAX_NORMAL_STRING_LEN, pName.trim().length()));
    }

    /**
     * Gibt das Kuerzel dieser Paedagogischen MitarbeiterIn zurueck.
     * 
     * @return das Kuerzel dieses Paedagogischen MitarbeiterIn
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * Setzt das Kuerzel dieser Paedagogischen MitarbeiterIn auf das -uebergebene Kuerzel. Falls das Kuerzel laenger als
     * {@linkplain Data#MAX_ACRONYM_LEN} Zeichen ist, wird es entsprechend gekuerzt. Fuehrende und folgende
     * Leerzeichen werden entfernt. Laest eine {@link IllegalArgumentException} aus, falls das Kuerzel leer ist.
     * 
     * Die systemweite Eindeutigkeit des Kuerzels wird hier NICHT geprueft!
     * 
     * @param pAcronym
     *            das neue Kuerzel dieser P�dagogischen MitarbeiterIn
     */
    public void setAcronym(final String pAcronym) {
        if (pAcronym == null || pAcronym.trim().isEmpty()) {
            throw new IllegalArgumentException("K�rzel der P�dagogischen MitarbeiterIn ist leer");
        }
        if(pAcronym.trim().length() != 3) throw new IllegalArgumentException("Das K�rzel musss eine l�nge von 3 haben");
        acronym = pAcronym.trim();
    }

    /**
     * Setzt die Unterrichts- bzw. Arbeitsstunden dieser Paedagogischen MitarbeiterIn auf den uebergebenen Wert. Laest eine
     * {@link IllegalArgumentException} aus, falls der Parameterwert kleiner als {@linkplain Paeda#MIN_HOURS_PER_WEEK}
     * oder groesser als {@linkplain Paeda#MAX_HOURS_PER_WEEK} ist.
     * 
     * @param pHoursPerWeek
     *            die neue Anzahl der Unterrichts- bzw. Arbeitsstunden dieser Paedagogischen MitarbeiterIn
     */
    public void setHoursPerWeek(final String pHoursPerWeek) {
    	if(pHoursPerWeek == null || pHoursPerWeek.trim().isEmpty()) {
            throw new IllegalArgumentException("Die Stunden pro Woche sind leer");
        }
    	int hours;
    	try{
    		hours = Integer.parseInt(pHoursPerWeek.trim());
    	}catch(NumberFormatException e){
    		throw new IllegalArgumentException("Bei Stunden pro Woche ist keine Zahl eingetragen");
    	}
        if (hours < MIN_HOURS_PER_WEEK || hours > MAX_HOURS_PER_WEEK) {
            throw new IllegalArgumentException("Arbeitsstunden ausserhalb der erlaubten Grenzen");
        }
        hoursPerWeek = hours;
    }

    /**
     * Gibt den Namen dieses Paedagogischen Mitarbeiters zurueck.
     * 
     * @return den Namen dieses Paedagogischen Mitarbeiters
     */
    public String getName() {
        return name;
    }

    /**
     * Gibt die Anzahl an Unterrichts- bzw. Arbeitsstunden dieser Paedagogischen MitarbeiterIn zurueck.
     * 
     * @return die Anzahl an Unterrichts- bzw. Arbeitsstunden dieser Paedagogischen MitarbeiterIn
     */
    public int getHoursPerWeek() {
        return hoursPerWeek;
    }

    /**
     * Gibt das Acronym, den Namen und die Wochenstundenzahl des Paedagogischen Mitarbeiters zur�ck.
     */
    @Override
    public String toString() {
        return String.format("Paeda [acronym=%s, name=%s, hpw=%s]", acronym, name, hoursPerWeek);
    }

    /**
     * Zwei Paedagogische MitarbeiterInnen sind dann gleich, wenn ihr Acronym gleich ist. 
     */
    
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Paeda)) {
            return false;
        }
        final Paeda that = (Paeda) other;
        return acronym.equals(that.acronym);
    }

        
    /**
     * Setzt den Wochenplan. Falls dieser null ist wird eine IllegalArgumentException
     * ausgel�st.
     * 
     * @param pWeekTable
     * 		Der zu setzende Wochenplan
     */
    public void setWeekTable(final WeekTable pWeekTable){
    	if(pWeekTable == null) throw new IllegalArgumentException("Der Wochenplan ist leer");
    	weekTable = pWeekTable;
    }
    
    /**
     * @return Den Wochenplan
     */
	public WeekTable getWeekTable(){
			return this.weekTable;
	}

	/**
	 * @return Gibt die Sichtbarkeit des P�dagogen wieders
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * Setzt die Sichbarkeit des P�dagogen
	 * @param enable
	 * 		Die Sichtbarkeit
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	/**
	 * Erh�ht die Stundenanzahl um 1 falls m�glich, sonst wird eine IllegalArgumentException
	 * ausgel�st
	 */
	public void currentHoursPerWeekPlusEins(){
		if(!hatNochZeit()) throw new IllegalArgumentException("Der P�dagoge hat keine Zeit Mehr �bgrig");
		currentHoursPerWeek++;
	}
	
	/**
	 * Verringert die stundenzahl um 1;
	 * wenn <0 fleigt eine exception
	 * @throws Exception 
	 */
	public void currentHoursPerWeekMinusEins(){
		if(currentHoursPerWeek > 0) currentHoursPerWeek--;
		else throw new IllegalArgumentException("current arbeitszeit < 0 . kann daher nicht verringert werden.");
	}
	
	/**
	 * Pr�ft ob eine person noch zeit hat.
	 * @return
	 *  true = Hat noch Zeit
	 *  false = Hat keine Zeit mehr
	 */
	public boolean hatNochZeit(){
		if(this.currentHoursPerWeek<Paeda.MAX_HOURS_PER_WEEK)return true;
		return false;
	}
	
}




