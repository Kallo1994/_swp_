package de.unibremen.swp.stundenplan.data;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import de.unibremen.swp.stundenplan.persistence.Data;

/**
 * Repr�sentiert eine LehrerIn. Eine LehrerIn hat einen Namen, ein systemweit eindeutiges K�rzel und eine Anzahl von
 * Stunden, die sie unterrichtet bzw. arbeitet.
 * 
 * Zwei LehrerInnen werden als gleich angesehen, wenn sie das gleiche K�rzel haben.
 * 
 * @author D. L�demann, K. H�lscher
 * #ge�ndert G�khan
 * @version 0.1
 */
@Entity
public final class Teacher implements Serializable {

    /**
     * Die generierte serialVersionUID.
     */
    private static final long serialVersionUID = -2391687797016927732L;

    /**
     * Die minimale Zahl an Unterrichts- bzw. Arbeitsstunden, die jede LehrerIn leisten muss. Kann nicht unterschritten
     * werden.
     */
    @Transient
    public static final int MIN_HOURS_PER_WEEK = 1;

    /**
     * Die maximale Zahl an Unterrichts- bzw. Arbeitsstunden, die jede LehrerIn leisten muss. Kann nicht �berschritten
     * werden.
     */
    @Transient
    public static final int MAX_HOURS_PER_WEEK = 40;
    
    /**
     * Die aktuelle Stundenzahl die diese Person pro Woche arbeitet.
     */
    private int currentHoursPerWeek;

    /**
     * Der Name dieser LehrerIn.
     */
    @Column(nullable = false, length = Data.MAX_NORMAL_STRING_LEN)
    private String name;

    /**
     * Das K�rzel dieser LehrerIn. Ein K�rzel muss systemweit eindeutig sein.
     */
    @Id
    @Column(length = Data.MAX_ACRONYM_LEN)
    private String acronym;

    /**
     * Die Anzahl an Stunden, die eine LehrerIn maximal unterrichtet bzw. arbeitet.
     */
    @Column
    private int hoursPerWeek;
    
    /**
     * Der Wochenplan des Lehrers
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    private WeekTable weekTable ;

    /**
     * Wenn enable auf "true", gilt der P�dagoge als Sichtbar.
     * Auf "false" gilt der P�dagoge als gel�scht und unsichtbar.
     */
    private boolean enable;
    
    /**
     * Setzt den Namen dieser LehrerIn auf den �bergebenen Namen. Falls der Name l�nger als
     * {@linkplain Data#MAX_NORMAL_STRING_LEN} Zeichen ist, wird er entsprechend gek�rzt. F�hrende und folgende
     * Leerzeichen werden entfernt. L�st eine {@link IllegalArgumentException} aus, falls der Name leer ist.
     * 
     * @param pName
     *            der neue Name dieser LehrerIn
     */
    public void setName(final String pName) {
        if (pName == null || pName.trim().isEmpty()) {
            throw new IllegalArgumentException("Der Name der LehrerIn ist leer");
        }
        name = pName.trim().substring(0, Math.min(Data.MAX_NORMAL_STRING_LEN, pName.trim().length()));
    }

    /**
     * Gibt das K�rzel dieser LehrerIn zur�ck.
     * 
     * @return das K�rzel dieses LehrerIn
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * Setzt das K�rzel dieser LehrerIn auf das �bergebene K�rzel. Falls das K�rzel l�nger als
     * {@linkplain Data#MAX_ACRONYM_LEN} Zeichen ist, wird es entsprechend gek�rzt. F�hrende und folgende
     * Leerzeichen werden entfernt. L�st eine {@link IllegalArgumentException} aus, falls das K�rzel leer ist.
     * 
     * Die systemweite Eindeutigkeit des K�rzels wird hier NICHT gepr�ft!
     * 
     * @param pAcronym
     *            das neue K�rzel dieser LehrerIn
     */
    public void setAcronym(final String pAcronym) {
        if (pAcronym == null || pAcronym.trim().isEmpty()) {
            throw new IllegalArgumentException("K�rzel der LehrerIn ist leer");
        }
        if(pAcronym.trim().length() != 3) throw new IllegalArgumentException("Das K�rzel musss eine l�nge von 3 haben");
        acronym = pAcronym.trim();
    }

    /**
     * Setzt die Unterrichts- bzw. Arbeitsstunden dieser LehrerIn auf den �bergebenen Wert. L�st eine
     * {@link IllegalArgumentException} aus, falls der Parameterwert kleiner als {@linkplain Teacher#MIN_HOURS_PER_WEEK}
     * oder gr��er als {@linkplain Teacher#MAX_HOURS_PER_WEEK} ist.
     * 
     * @param pHoursPerWeek
     *            die neue Anzahl der Unterrichts- bzw. Arbeitsstunden dieser LehrerIn
     */
    public void setHoursPerWeek(final String pHoursPerWeek) {
    	if(pHoursPerWeek == null || pHoursPerWeek.trim().isEmpty()) {
            throw new IllegalArgumentException("Die Stunden pro Woche sind leer");
        }
    	int hours;
    	try{
    		hours = Integer.parseInt(pHoursPerWeek.trim());
    	}catch(NumberFormatException e){
    		throw new IllegalArgumentException("Bei Stunden pro Woche ist keine Zahl eingetragen");
    	}
        if (hours < MIN_HOURS_PER_WEEK || hours > MAX_HOURS_PER_WEEK) {
            throw new IllegalArgumentException("Arbeitsstunden ausserhalb der erlaubten Grenzen");
        }
        hoursPerWeek = hours;
    }

    /**
     * @return den Namen dieses Lehrers
     */
    public String getName() {
        return name;
    }

    /**
     * Gibt die Anzahl an Unterrichts- bzw. Arbeitsstunden dieser LehrerIn zur�cck.
     * 
     * @return die Anzahl an Unterrichts- bzw. Arbeitsstunden dieser LehrerIn
     */
    public int getHoursPerWeek() {
        return hoursPerWeek;
    }

    @Override
    public String toString() {
        return String.format("Teacher [acronym=%s, name=%s, hpw=%s]", acronym, name, hoursPerWeek);
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Teacher)) {
            return false;
        }
        final Teacher that = (Teacher) other;
        return acronym.equals(that.acronym);
    }

    /**
     * Setzt den Wochenplan. Falls dieser null ist wird eine IllegalArgumentException
     * ausgel�st.
     * 
     * @param pWeekTable
     * 		Der zu setzende Wochenplan
     */
    public void setWeekTable(final WeekTable pWeekTable){
    	if(pWeekTable == null) throw new IllegalArgumentException("Der Wochenplan ist leer");
    	weekTable = pWeekTable;
    }
    
    /**
     * @return Den Wochenplan
     */
	public WeekTable getWeekTable(){
			return this.weekTable;
	}

	/**
	 * @return Gibt die Sichtbarkeit des P�dagogen wieders
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * Setzt die Sichbarkeit des P�dagogen
	 * @param enable
	 * 		Die Sichtbarkeit
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	/**
	 * Erh�ht die Stundenanzahl um 1 falls m�glich, sonst wird eine IllegalArgumentException
	 * ausgel�st
	 */
	public void currentHoursPerWeekPlusEins(){
		if(!hatNochZeit()) throw new IllegalArgumentException("Der P�dagoge hat keine Zeit Mehr �bgrig");
		currentHoursPerWeek++;
	}
	
	/**
	 * Verringert die stundenzahl um 1;
	 * wenn <0 fleigt eine exception
	 * @throws Exception 
	 */
	public void currentHoursPerWeekMinusEins(){
		if(currentHoursPerWeek > 0) currentHoursPerWeek--;
		else throw new IllegalArgumentException("current arbeitszeit < 0 . kann daher nicht verringert werden.");
	}
	
	/**
	 * Pr�ft ob eine person noch zeit hat.
	 * @return
	 *  true = Hat noch Zeit
	 *  false = Hat keine Zeit mehr
	 */
	public boolean hatNochZeit(){
		if(this.currentHoursPerWeek<Paeda.MAX_HOURS_PER_WEEK)return true;
		return false;
	}
}
