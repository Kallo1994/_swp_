/*
 * Copyright 2014 AG Softwaretechnik, University of Bremen, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.unibremen.swp.stundenplan.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import de.unibremen.swp.stundenplan.config.*;

/**
 * Repr�sentiert einen Tagesplan eines Stundenplans an einem bestimmten Wochentag. Verwaltet eine Liste von
 * Zeiteinheiten. Jeder Tagesplan verwaltet innerhalb eines Stundenplans die gleiche Anzahl von Zeiteinheiten. Diese
 * Anzahl ist konfigurierbar und per Default auf {@linkplain Config#DAYTABLE_LENGTH_DEFAULT} festgelegt. Alle Tagespl�ne
 * beginnen zur gleichen konfigurierbaren Uhrzeit. Diese Startzeit ist per Default festgelegt auf die Stunde
 * {@linkplain Config#DAYTABLE_STARTTIME_HOUR_DEFAULT} und die Minute
 * {@linkplain Config#DAYTABLE_STARTTIME_MINUTE_DEFAULT}. Die Endzeit des Tagesplans ergibt sich dann aus der Startzeit
 * und der Anzahl der Zeiteinheiten.
 * 
 * @author Karsten H�lscher
 * #ge�ndet G�khan
 * @version 0.1
 */
@Entity
public final class DayTable {

    /**
     * Anzahl der Zeiteinheiten, die alle Tagespl�ne enthalten.
     */
    @Transient
    public static final int LENGTH = Config.getInt(Config.DAYTABLE_LENGTH_STRING,
            Config.DAYTABLE_LENGTH_DEFAULT);

    /**
     * Die Stunde, zu der ein Tagesplan beginnt.
     */
    @Transient
    public static final int STARTTIME_HOUR = Config.getInt(Config.DAYTABLE_STARTTIME_HOUR_STRING,
            Config.DAYTABLE_STARTTIME_HOUR_DEFAULT);

    /**
     * Die Minute, zu der ein Tagesplan beginnt.
     */
    @Transient
    public static final int STARTTIME_MINUTE = Config.getInt(Config.DAYTABLE_STARTTIME_MINUTE_STRING,
            Config.DAYTABLE_STARTTIME_MINUTE_DEFAULT);

    /**
     * Die ID dieses Tagesplans. Wird von der Persistenz automatisch generiert und dadurch die Eindeutigkeit
     * sichergestellt.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Die Liste von Zeiteinheiten, aufsteigend sortiert nach ihren Anfangszeiten.
     */
    @OneToMany(cascade = CascadeType.PERSIST)
    @OrderBy("startTime ASC")
    private List<Timeslot> timeslots = new ArrayList<>();

    /**
     * Der Wochentag dieses Tagesplans.
     */
    @Enumerated(EnumType.STRING)
    private Weekday weekday;

    /**
     * F�gt die �bergebene Zeiteinheit zu diesem Tagesplan hinzu. L�st eine {@link IllegalArgumentException} aus, falls
     * die �bergebene Zeiteinheit {@code null} ist
     * 
     * @param pTimeslot
     *            die hinzuzuf�gende Zeiteinheit
     */
    public void addTimeslot(final Timeslot pTimeslot) {
        if (pTimeslot == null) throw new IllegalArgumentException("TimeSlot darf nicht null sein");
        timeslots.add(pTimeslot);
    }

    /**
     * Gibt den Wochentag dieses Tagesplans zur�ck.
     * 
     * @return den Wochentag dieses Tagesplans
     */
    public Weekday getWeekday() {
        return weekday;
    }

    /**
     * Gibt die Zeiteinheit am gegebenen Positionsindex zur�ck oder {@code null} falls ein ung�ltiger Positionsindex
     * zur�ckgegeben wurde. Die erste Zeiteinheit des Tagesplans beginnt bei Index 0.
     * 
     * @param position
     *            die Position der gesuchten Zeiteinheit
     * @return die gesuchte Zeiteinheit oder {@code null}, falls eine ung�ltige Position �bergeben wurde
     */
    public Timeslot getTimeslot(final int position) {
        if (position >= 0 && position < timeslots.size()) {
            return timeslots.get(position);
        } else {
            return null;
        }
    }
    
    /**
     * Setzt den Wochentag des Tagesplanes. L�st eine {@link IllegalArgumentException} aus, falls
     * der �bergebene Wochentag  {@code null} ist.
     * @param pweekday
     * 		Der Wochentag f�r den Tagesplan
     */
    public void setWeekday(Weekday pweekday){
    	if(pweekday==null)throw new IllegalArgumentException("Der Wochentag darf nicht null sein");
    	weekday=pweekday;
    }

}
