/*
 * Copyright 2014 AG Softwaretechnik, University of Bremen, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package de.unibremen.swp.stundenplan.data;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import de.unibremen.swp.stundenplan.config.Config;

/**
 * Entspricht einer Zeiteinheit. Eine Zeiteinheit ist einem Tagesplan zugeordnet
 * und hat eine Startzeit. Die Dauer einer solchen Zeiteinheit ist
 * konfigurierbar und per Default auf
 * {@linkplain Config#TIMESLOT_LENGTH_DEFAULT} festgelegt.
 * 
 * @author D. Loedemann, K. Hoelscher
 * @version 0.1
 * 
 */
@Entity
public final class Timeslot implements Serializable {

	/**
	 * Die generierte serialVersionUID.
	 */
	private static final long serialVersionUID = 4249954963688259056L;

	/**
	 * Die Dauer aller Zeiteinheiten in Minuten.
	 */
	@Transient
	public static final int LENGTH = Config.getInt(
			Config.TIMESLOT_LENGTH_STRING, Config.TIMESLOT_LENGTH_DEFAULT);

	/**
	 * Die eindeutige, von der unterliegenden Persistenzschicht automatisch
	 * erzeugte ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Jedem Timeslot wird eine Aktivitaet zugeordnet. 
	 */
	@OneToOne(cascade = CascadeType.PERSIST)
	private Aktivitaet aktivitaet;
	
	/**
	 * Die Startzeit dieses Timeslots. Die Eintraege fuer
	 * {@linkplain Calendar#HOUR} und {@linkplain Calendar#MINUTE} muessen
	 * entsprechend gesetzt sein.
	 */
	@Temporal(TemporalType.TIME)
	private Calendar startTime;

	/**
	 * Setzt die Startzeit dieser Zeiteineit auf den Uebergebenen Wert. Die
	 * Eintraege fuer {@linkplain Calendar#HOUR} und
	 * {@linkplain Calendar#MINUTE} muessen entsprechend gesetzt sein. Der
	 * Parameterwert wird nicht auf Plausibilit�t geprueft - {@code null} wird
	 * allerdings ignoriert.
	 * 
	 * @param pStartTime
	 *            Startzeit dieser Zeiteinheit (es sind nur die Eintraege fuer
	 *            {@linkplain Calendar#HOUR} und {@linkplain Calendar#MINUTE}
	 *            relevant
	 */
	public void setStartTime(final Calendar pStartTime) {
		if (pStartTime != null) 
			startTime = pStartTime;
		
	}

	/**
	 * Gibt die Startzeit dieser Zeiteinheit im Format <stunde>:<minute> mit
	 * evtl. fuehrenden Nullen zurueck oder einen leeren String, falls die
	 * Startzeit noch nicht initialisiert wurde.
	 * 
	 * @return die Startzeit dieser Zeiteinheit im Format <stunde>:<minute> mit
	 *         evtl. fuehrenden Nullen
	 */
	public String getTimeDisplay() {
		if (startTime == null) {
			return "";
		}
		final int hour = startTime.get(Calendar.HOUR);
		final int minute = startTime.get(Calendar.MINUTE);
		return String.format("%02d:%02d", hour, minute);
	}
	
	/**
	 * L�scht die Aktivit�t, setzt sie also auf null.
	 * Ist die Aktivit�t bereits null, passiert nichts.
	 */
	public void loescheAktivitaet() {
		this.aktivitaet = null;
	}

	/**
	 * @return Die Aktivit�t
	 */
	public Aktivitaet getAktivitaet() {
		return aktivitaet;
	}

	/**
	 * @return Die Startzeit
	 */
	public Calendar getStartTime() {
		return startTime;
	}

	/**
	 * Setzt eine neue Aktivi�t
	 * @param pAktivitaet
	 */
	public void setAktivitaet(final Aktivitaet pAktivitaet){
		aktivitaet = pAktivitaet;
	}
}
