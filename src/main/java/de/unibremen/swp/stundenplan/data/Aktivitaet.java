package de.unibremen.swp.stundenplan.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * In jedem Timeslot befindet sich eine Aktivit�t. Eine aktivit�t beeinhaltet
 * mehrere Klassen und Personen.
 * 
 * @author Henrik
 *
 */
@Entity
public final class Aktivitaet implements Serializable {

	/**
	 *  Die eineindeutige ID f�r Serialisierung.
	 */
	private static final long serialVersionUID = 2905723126017756431L;

	/**
	 * Die ID der Aktivit�t. Wird von der Persistenz automatisch generiert und
	 * dadurch die Eindeutigkeit sichergestellt.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Die Bezeichnung der Aktivit�t.
	 */
	private String bezeichnung;

	/**
	 * Diese Liste enth�lt alle Schulklassen die an der Aktivit�t teilnehmen.
	 */
	@OneToMany(cascade = CascadeType.PERSIST)
	private List<Schoolclass> classes = new ArrayList<>();

	/**
	 * Diese Liste enth�lt alle Lehrer die an der Aktivit�t teilnehmen.
	 */
	@OneToMany(cascade = CascadeType.PERSIST)
	private List<Teacher> teachers = new ArrayList<>();

	/**
	 * Diese Liste enth�lt alle P�dagogischen Mitarbeiter die an der Aktivit�t
	 * teilnehmen.
	 */
	@OneToMany(cascade = CascadeType.PERSIST)
	private List<Paeda> paedas = new ArrayList<>();

	/**
	 * @return Die Liste der Schulklassen, die an der Aktivit�t teilnehmen.
	 */
	public List<Schoolclass> getClasses() {
		return classes;
	}

	/**
	 * @return Die Liste der Lehrer die an der Aktivit�t teilnehmen.
	 */
	public List<Teacher> getTeachers() {
		return teachers;
	}

	/**
	 * @return Die Liste der P�dagogischen Mitarbeiter, die an der Aktivit�t
	 *         teilnehmen.
	 */
	public List<Paeda> getPaedas() {
		return paedas;
	}

	/**
	 * F�gt einen Lehrer zu der Liste der teilnehmenden Lehrer hinzu. Ist der
	 * lehrer schon in der liste, oder ist der �bergebene Lehrer null wird
	 * eine IllegalArgumentException gewurfen.
	 */
	public void addTeacher(final Teacher pTeacher) {
		if(pTeacher == null)throw new IllegalArgumentException("Der Lehrer darf nicht null sein");
		if(teachers.contains(pTeacher)) throw new IllegalArgumentException("Der Lehrer ist schon bei dieser Aktivit�t");
		teachers.add(pTeacher);

	}

	/**
	 * F�gt eine Schoolclass zu der Liste der teilnehmenden Schoolclassn hinzu.
	 * ist die klasse schon in der liste, oder ist die �bergebene Klasse null wird
	 * eine IllegalArgumentException gewurfen.
	 */
	public void addClass(final Schoolclass pClass) {
		if(pClass == null)throw new IllegalArgumentException("Die Schulklasse darf nicht null sein");
		if(classes.contains(pClass)) throw new IllegalArgumentException("Die Schulklasse ist schon bei dieser Aktivit�t");
		classes.add(pClass);
	}

	/**
	 * F�gt einen P�dagogischen Mitarbeiter zu der Liste der teilnehmenden
	 * P�dagogischen Mitarbeiter hinzu. ist der P�dagogischen Mitarbeiter schon
	 * in der liste, oder ist der �bergebene P�dagogischen Mitarbeiter null wird
	 * eine IllegalArgumentException gewurfen.
	 */
	public void addPaeda(final Paeda pPaeda) {
		if(pPaeda == null)throw new IllegalArgumentException("Der P�dagoge darf nicht null sein");
		if(paedas.contains(pPaeda)) throw new IllegalArgumentException("Der P�dagoge ist schon bei dieser Aktivit�t");
		paedas.add(pPaeda);
	}

	/**
	 * Entfernt einen Lehrer aus der Liste aller teilnehmenden Lehrer. Ist der
	 * Lehrer nicht in der liste oder der �bergebene lehrer null, wird
	 * eine IllegalArgumentException gewurfen.
	 * @param pTeacher
	 *            der zu entfernende Lehrer
	 */
	public void removeTeacher(final Teacher pTeacher) {
		if(pTeacher == null)throw new IllegalArgumentException("Der Lehrer darf nicht null sein");
		if(!teachers.contains(pTeacher)) throw new IllegalArgumentException("Der Lehrer ist nicht in dieser Aktivit�t");
		teachers.remove(pTeacher);
	}

	/**
	 * Entfernt eine Schoolclass aus der Liste aller teilnehmenden Schoolclassn
	 * ist die klasse nicht in der liste, oder die �bergebene klasse null,wird
	 * eine IllegalArgumentException gewurfen.
	 * 
	 * @param pClass
	 *            die zu entfernende Schoolclass
	 */
	public void removeClass(final Schoolclass pClass) {
		if(pClass == null)throw new IllegalArgumentException("Die Schulklasse darf nicht null sein");
		if(classes.contains(pClass)) throw new IllegalArgumentException("Die Schulklasse ist nicht in dieser Aktivit�t");
		classes.remove(pClass);
	}

	/**
	 * Entfernt einen P�dagogischen Mitarbeiter aus der Liste aller
	 * teilnehmenden paedagogischen Mitarbeiter. ist der paedagogischen
	 * Mitarbeiter nicht in der liste oder der �bergebene paedagogischen
	 * Mitarbeiter null, wird eine IllegalArgumentException gewurfen.
	 * 
	 * @param pPaeda
	 *            der zu entfernende paedagogische Mitarbeiter
	 */
	public void removePaeda(final Paeda pPaeda) {
		if(pPaeda == null)throw new IllegalArgumentException("Der P�dagoge darf nicht null sein");
		if(paedas.contains(pPaeda)) throw new IllegalArgumentException("Der P�dagoge ist nicht in dieser Aktivit�t");
		paedas.remove(pPaeda);
	}

	/**
	 * Gibt die dieser Zeiteinheit zugeordneten LehrerInnen als Liste ihrer
	 * K�rzel, getrennt durch Komma, zur�ck.
	 * 
	 * @return die dieser Zeiteinheit zugeordneten LehrerInnen als
	 *         kommaseparierte Liste
	 */
	public String getTeacherAcronymList() {
		final StringBuilder teacherString = new StringBuilder();
		String separator = "";
		for (final Teacher teacher : teachers) {
			teacherString.append(separator);
			teacherString.append(teacher.getAcronym());
			separator = ", ";
		}
		return teacherString.toString();
	}

	/**
	 * Gibt dieser Ativit�t zugeordneten P�dagogenInnen als Liste hrer
	 * K�rzel, getrennt durch Komma, zur�ck.
	 * 
	 * @return die dieser Zeiteinheit zugeordneten P�dagogenInnen als
	 *         kommaseparierte Liste
	 */
	public String getPaedaAcronymList() {
		final StringBuilder paedaString = new StringBuilder();
		String separator = "";
		for (final Paeda paeda : this.paedas) {
			paedaString.append(separator);
			paedaString.append(paeda.getAcronym());
			separator = ", ";
		}
		return paedaString.toString();
	}

	/**
	 * gibt die namen der Klassen diendieser aktivit�t als Liste hrer
	 * K�rzel, getrennt durch Komma, zur�ck.
	 * 
	 * @return die dieser Zeiteinheit zugeordneten Aktivit�t als
	 *         kommaseparierte Liste
	 */
	public String getKlasseAcronymList() {
		final StringBuilder klasseString = new StringBuilder();
		String separator = "";
		for (final Schoolclass klasse : this.classes) {
			klasseString.append(separator);
			klasseString.append(klasse.getName());
			separator = ", ";
		}
		return klasseString.toString();
	}

	/**
	 * Setzt die Bezeichnung der Aktivit�t, Falls dieser null ist wird eine 
	 * IllegalArgumentException gewurfen.
	 * 
	 * @param s
	 * 		Die �bergebene Bezeichnung
	 */
	public void setBezeichnung(String s) {
		if (s == null)throw new IllegalArgumentException("Die Bezeichnung darf nicht null sein!");
		bezeichnung = s;
			
	}

	@Override
	public String toString() {
		return String.format("Timeslot [teachers=%s,schoolclasses=%s",
				getTeacherAcronymList(), classes);
	}

	/**
	 * @return Die Bezeichnung der Aktivit�t
	 */
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	/**
	 * Gibe eine gesuchte Schulklasse wieder, falls der dieser bei der 
	 * Aktivit�t teilnimmst
	 * @param Name
	 * 		Der name der gesuchte Schulklasse
	 * @return
	 * 		Die Schulklasse
	 */
	public Schoolclass getKlasseByName(final String Name) {
		for (Schoolclass klasse : classes) {
			if(klasse.getName().equals(Name)){
				return klasse;
			}
		}
		return null;
	}
	
	/**
	 * Gibe einen gesuchten Lehrer wieder, falls dieser an der 
	 * Aktivit�t teilnimmst
	 * @param Name
	 * 		Der name des gesuchten Lehrers
	 * @return
	 * 		Der Lehrer
	 */
	public Teacher getTeacherByaAcronym(final String acronym) {
		for (Teacher teacher : teachers) {
			if(teacher.getName().equals(acronym)){
				return teacher;
			}
		}
		return null;
	}
	
	/**
	 * Gibe einen gesuchten P�dagogen wieder, falls dieser an der 
	 * Aktivit�t teilnimmst
	 * @param Name
	 * 		Der name des gesuchten P�dagogens
	 * @return
	 * 		Der P�dagoge
	 */
	public Paeda getPaedaByaAcronym(final String acronym) {
		for (Paeda paeda : paedas) {
			if(paeda.getName().equals(acronym)){
				return paeda;
			}
		}
		return null;
	}
}
