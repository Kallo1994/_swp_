package de.unibremen.swp.stundenplan.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import de.unibremen.swp.stundenplan.config.Weekday;

/**
 * Stellt einen Wochenplan einer person/klasse dar. Ein Wochenplan besteht aus 5
 * tagespl�nen.
 * 
 * @author Kai
 *
 */
@Entity
public class WeekTable {

	/**
	 * Die ID dieses Wochenplans. Wird von der Persistenz automatisch generiert
	 * und dadurch die Eindeutigkeit sichergestellt.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * Ein Wochenplan besteht aus 5 tagespl�nen (mo-fr)
	 */
	@OneToMany(cascade = CascadeType.PERSIST)
	private List<DayTable> daytables = new ArrayList<>();

	/**
	 * Erstellt einen neuen Weektable der leer ist.
	 */
	public WeekTable() {
	}

	/**
	 * F�gt den �bergebeben Daytable den Wochenplan hinzu.
	 * 
	 * @param dt
	 *            der Daytable der hinzugef�gt werden soll
	 */
	public void addDayTable(final DayTable dt) {
		if (dt == null)throw new IllegalArgumentException("�bergebener daytable == null ");
		daytables.add(dt);
	}
	
	/**
	 * Gibt einen DayTable wieder f�r einen gegebenen Tag.
	 * Ist f�r den gegebenen Tag kein DayTable vorhanden, wird null zur�ckgegeben.
	 * 
	 * 
	 * @param weekday
	 * 		der gesuchte Tag
	 * @return
	 * 		Das gefundene DayTable
	 */
	public DayTable getDayTableforDay(Weekday weekday){
		for(DayTable db : daytables){
			if(db.getWeekday() == weekday){
				return db;
			}
		}
		return null;
	}
}
