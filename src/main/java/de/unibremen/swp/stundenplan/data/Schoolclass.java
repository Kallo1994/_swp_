package de.unibremen.swp.stundenplan.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Entspricht einer Schulklasse. Eine Schulklasse hat einen Namen.
 * 
 * @author D. L�demann
 * #ge�ndert Kai
 * @version 0.1
 */
@Entity
public final class Schoolclass implements Serializable {

    /**
     * Die eineindeutige ID f�r Serialisierung.
     */
    private static final long serialVersionUID = 3137139574206115533L;

    /**
     * Der Name dieser Schulklasse.
     */
    @Id
    private String name;
    
    /**
     * Der Wochenplan der Klasse
     */
    @OneToOne
    private WeekTable weekTable ;
    
    /**
     * Wenn enable auf "true", gilt der P�dagoge als Sichtbar.
     * Auf "false" gilt der P�dagoge als gel�scht und unsichtbar.
     */
    private boolean enable;

    /**
     * Gibt den Namen dieser Schulklasse zur�ck.
     * 
     * @return den Namen dieser Schulklasse
     */
    public String getName() {
        return name;
    }
    
    /**
     * Setzt den Namen dieser Schulklasse auf den gegebenen Wert.
     * 
     * @param pName
     *            der neue Name dieser Schulklasse (falls nicht {@code null} wird eine
     *            IllegalArgumentException ausgel�st
     */
    public void setName(final String pName) {
    	if (pName == null || pName.trim().isEmpty()) {
            throw new IllegalArgumentException("Der Name der Schulklasse ist leer");
        }
    	if(pName.trim().length() != 2) throw new IllegalArgumentException("Der Name der Klasse stimmt nicht");
        name = pName.trim();
    }
    
    /**
     * Setzt den Wochenplan. Falls dieser null ist wird eine IllegalArgumentException
     * ausgel�st.
     * 
     * @param pWeekTable
     * 		Der zu setzende Wochenplan
     */
    public void setWeekTable(final WeekTable pWeekTable){
    	if(pWeekTable == null) throw new IllegalArgumentException("Der Wochenplan ist leer");
    	weekTable = pWeekTable;
    }
    
    /**
     * @return Den Wochenplan
     */
	public WeekTable getWeekTable(){
		return weekTable;
	}

	/**
	 * @return Gibt die Sichtbarkeit des P�dagogen wieders
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * Setzt die Sichbarkeit des P�dagogen
	 * @param enable
	 * 		Die Sichtbarkeit
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
}
